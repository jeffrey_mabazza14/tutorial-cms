<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title>Cortex | Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
{{-- BEGIN GLOBAL MANDATORY STYLES --}}
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
{{-- END GLOBAL MANDATORY STYLES --}}
{{-- BEGIN PAGE LEVEL STYLES --}}
<link href="{{ asset('assets/admin/pages/css/login.css') }}" rel="stylesheet" type="text/css"/>
{{-- END PAGE LEVEL SCRIPTS --}}
{{-- BEGIN THEME STYLES --}}
<link href="{{ asset('assets/global/css/components.css') }}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/layout/css/layout.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/layout/css/themes/darkblue.css') }}" rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{ asset('assets/admin/layout/css/custom.css') }}" rel="stylesheet" type="text/css"/>
{{-- END THEME STYLES --}}
<link rel="shortcut icon" href="favicon.ico"/>
<style>
	.force-hide-element {
		display: none !important;
	}
</style>
</head>
{{-- END HEAD --}}
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="javascript:;">
	@if(in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1', '192.168.10.1', '180.232.124.116']))
		<img style="width: 140px !important; margin-bottom: -45px" src="{{ asset('assets/logos/cortex_logo_061015.png') }}" alt=""/>
	@else
		<img style="width: 140px !important; margin-bottom: -45px" src="{{ asset('assets/logos/01_w_cortex_logo_061715.png') }}" alt=""/>
	@endif
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	{!! Form::open(['url'=>url('auth'), 'class'=>'login-form', 'method'=>'POST']) !!}
		<h3 class="form-title">Sign In</h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Enter any username and password. </span>
		</div>
		@if(session()->has('error'))
			<div class="alert alert-danger" id="alert-error-laravel">
				<button class="close" data-close="alert"></button>
				<span>
				{{ session('error')['message'] }} </span>
			</div>
		@elseif(session()->has('success'))
			<div class="alert alert-success" id="alert-success-laravel">
				<button class="close" data-close="alert"></button>
				<span>
				{{ session('success')['message'] }} </span>
			</div>
		@endif
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Email</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="username"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase" id="login-btn" onclick="jQuery('#alert-error-laravel').addClass('force-hide-element');">Login</button>
			<label></label>
			{{-- <label class="rememberme check"> --}}
			{{-- <input type="checkbox" name="remember" value="1"/>Remember </label> --}}
			<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
		</div>
		{{-- <div class="create-account">
			<p>
				<a href="javascript:;" id="register-btn" class="uppercase">Create an account</a>
			</p>
		</div> --}}
	{!! Form::close() !!}
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->
	<form class="forget-form" method="post" action="forgot-password" onsubmit="return validateForgotPassword()">
		<h3>Forget Password ?</h3>
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="alert alert-danger display-hide" id="forgot-password-alert">
			<button class="close" data-close="alert"></button>
			<span>
			</span>
		</div>
		<div class="form-group">
			<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="forgot-input-email"/>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn btn-default">Back</button>
			<button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->
	<!-- BEGIN REGISTRATION FORM -->
	{!! Form::open(['url'=>'create-user', 'class'=>'register-form', 'method'=>'POST']) !!}
		<h3>Sign Up</h3>
		<p class="hint">
			 Enter your personal details below:
		</p>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">First Name</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="firstname"/>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Last Name</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="lastname"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Email / Username</label>
			<input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email"/>
		</div>
		<p class="hint">
			 Enter your account details below:
		</p>
		{{-- <div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/>
		</div> --}}
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
			<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="rpassword"/>
		</div>
		{{-- <div class="form-group margin-top-20 margin-bottom-20">
			<label class="check">
			<input type="checkbox" name="tnc"/> I agree to the <a href="javascript:;">
			Terms of Service </a>
			& <a href="javascript:;">
			Privacy Policy </a>
			</label>
			<div id="register_tnc_error">
			</div>
		</div> --}}
		<div class="form-actions">
			<button type="button" id="register-back-btn" class="btn btn-default">Back</button>
			<button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
		</div>
	{!! Form::close() !!}
	<!-- END REGISTRATION FORM -->
</div>
<div class="copyright">
	 {{-- Cortex --}}
</div>
<!-- END LOGIN -->
{{-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) --}}
{{-- BEGIN CORE PLUGINS --}}
<!--[if lt IE 9]>
<script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script> 
<![endif]-->
<script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
{{-- END CORE PLUGINS --}}
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/pages/scripts/login.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
<script type="text/javascript">
	function validateForgotPassword() {

		var error = false;
		var element = $('#forgot-input-email');
		var format = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;


		if($.trim(element.val()) == '') {
			error = 'Please enter your email address.';
			element.focus();
		} else if( ! format.test($.trim(element.val()))) {
			error = 'Please enter a valid email address.';
			element.focus();
		}

		if(error) {
			$('#forgot-password-alert').show();
			$('#forgot-password-alert span').html(error);
			return false;
		} else {
			return true;
		}
	}
</script>
<script>
jQuery(document).ready(function() {     
Metronic.init();
Layout.init();
Login.init();
Demo.init();
});
</script>
{{-- END JAVASCRIPTS --}}
</body>
{{-- END BODY --}}
</html>