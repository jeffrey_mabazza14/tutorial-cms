@extends('layouts.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/icheck/skins/all.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/select2/select2.css') }}"/>
@stop

@section('content')
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Home</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa icon-user"></i>
						<span>Users</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box yellow-lemon">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-user"></i>Users <span class="badge badge-alert">{{ $total }} </span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-bordered">
								<thead>
								<tr>
									<th>
										 #
									</th>
									{{-- <th>
										 Username
									</th> --}}
									<th>
										 Firstname
									</th>
									<th>
										 Lastname
									</th>
									<th>
										 Email
									</th>
									{{-- <th>
										 Status
									</th> --}}
									<th>
										 Date Registered
									</th>
									<th>
										 Action
									</th>
								</tr>
								</thead>
								<tbody>
								@if($users)
									<?php $i = 1; ?>
									@foreach($users as $user)
										<tr id="row-{{ $i }}">
											<td>{{ $i }}</td>
											{{-- <td>{{ $user->username }}</td> --}}
											<td>{{ $user->firstname }}</td>
											<td>{{ $user->lastname }}</td>
											<td>{{ $user->email }}</td>
											{{-- <td>
												@if($user->status == 0)
													<span class="label label-sm label-info">
													Pending </span>
												@elseif($user->status == 1)
													<span class="label label-sm label-success">
													Approved </span>
												@elseif($user->status == 2)
													<span class="label label-sm label-danger">
													Rejected </span>
												@elseif($user->status == 3)
													<span class="label label-sm label-danger">
													Duplicate Email </span>
												@endif
											</td> --}}
											<td>{{ $user->created_at }}</td>
											<td>
												<a class="btn default btn-xs red" href="#remove-{{ $i }}" role="button" data-toggle="modal">
												<i class="fa fa-thumbs-up"></i> Remove </a>
											</td>
											{{-- <td>
												@if($user->status != 1 && $user->status != 3)
													<a class="btn default btn-xs green" href="#approve-{{ $i }}" role="button" data-toggle="modal">
													<i class="fa fa-thumbs-up"></i> Approve </a>
												@endif
												@if($user->status != 2)
												<a href="#reject-{{ $i }}" class="btn default btn-xs red" role="button" data-toggle="modal">
												<i class="fa fa-thumbs-down"></i> Reject </a>
												@endif
											</td> --}}
										</tr>

										{{-- Modal --}}
										<div id="approve-{{ $i }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Are you sure you want to APPROVE this user?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														<a href="{{ url('users/manage').'/'.$user->id.'?action=approve' }}" type="button" class="btn green">Yes</a>
													</div>
												</div>
											</div>
										</div>

										<div id="reject-{{ $i }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Are you sure you want to REJECT this user?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														<a href="{{ url('users/manage').'/'.$user->id.'?action=suspend' }}" type="button" class="btn green">Yes</a>
													</div>
												</div>
											</div>
										</div>

										<div id="remove-{{ $i }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Are you sure you want to REMOVE this user?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														<a href="{{ url('users/manage').'/'.$user->id.'?action=remove' }}" type="button" class="btn green">Yes</a>
													</div>
												</div>
											</div>
										</div>
									<?php $i++; ?>
									@endforeach
								@else
									<tr>
										<td colspan="10" style="text-align: center">No Record(s) Found.</td>
									</tr>
								@endif
								
								</tbody>
								</table>
								{{-- Pagination Links --}}
								@if($users)
									<div class="dataTables_paginate paging_simple_numbers">
										{!! $users->render() !!}
									</div>
								@endif
								{{-- Pagination Links --}}
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
@stop
@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
@stop
@section('defined-scripts')
	<script>
	jQuery(document).ready(function() {       
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
	});
	</script>
@stop