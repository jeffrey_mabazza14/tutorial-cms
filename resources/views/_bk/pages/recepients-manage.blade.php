@extends('layouts.default')
@section('styles')
	<link href="{{ asset('assets/global/plugins/icheck/skins/all.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Home</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa icon-briefcase"></i>
						<span>Recipients</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa icon-user"></i>
						<span>Manage</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box yellow-lemon">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-user"></i>Recipients - Manage <span class="badge badge-alert">{{ @$total }} </span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-bordered">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 Recepient Name
									</th>
									<th>
										 Number of Contacts
									</th>
									<th>
										 Action
									</th>
								</tr>
								</thead>
								<tbody>
								@if($lists)
									<?php $offset = isset($_GET['page']) && $_GET['page'] && $_GET['page'] != 1 ? $_GET['page'] * 15 - 15 : NULL ?>
									@foreach($lists as $k => $v)
										<tr>
											<td>
												<?php $offset = ($offset === NULL ? $k : $offset) + 1 ?>
												{{ $offset }}
											</td>
											<td>{{ str_replace('_', ' ', $v['table_name']) }}</td>
											<td>{{ DB::table('client_' . $v['table_name'])->count() }}</td>
											<td>
												<a class="btn default btn-xs green" href="#confirm-{{ $k }}" role="button" data-toggle="modal">
												<i class="fa fa-edit"></i> Edit </a>
												{{-- <a href="#delete-{{ $k }}" class="btn default btn-xs red" role="button" data-toggle="modal">
												<i class="fa fa-trash-o"></i> Remove </a> --}}
											</td>
										</tr>

										{{-- Confirmation Modal --}}
										<div id="confirm-{{ $k }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Would you like to continue editing this item?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														<a class="btn green" href="{{ url('recepients/edit') .'/'. $v['table_name'] }}">Continue</a>
													</div>
												</div>
											</div>
										</div>

										<div id="delete-{{ $k }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Would you like to continue deleting this item?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														<a class="btn green" href="{{ url('recepients/delete') . '/' . $v['table_name'] }}">Continue</a>
													</div>
												</div>
											</div>
										</div>
										{{-- Confirmation Modal --}}
									@endforeach
								@else
									<tr>
										<td colspan="10" style="text-align: center">No Record(s) Found.</td>
									</tr>
								@endif
								
								</tbody>
								</table>
								<div class="dataTables_paginate paging_simple_numbers yellow-lemon">
									{!! $lists->render() !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/icheck/icheck.min.js') }}" type="text/javascript"></script>
@stop
@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/form-wizard.js') }}" type="text/javascript"></script>
@stop
@section('defined-scripts')
	<script>
	jQuery(document).ready(function() {       
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		FormWizard.init();
	});
	</script>
@stop