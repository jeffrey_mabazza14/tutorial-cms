<table class="table table-bordered table-condensed flip-content">
	@if($table_header)
		<input name="table_name" value="{{ $table_name }}" type="hidden">
		<thead class="flip-content">
			<tr>
				@foreach($table_header as $k => $v)
					<th>{{ ucwords(str_replace('_', ' ', $v)) }}</th>
				@endforeach
			</tr>
			</thead>
			<tbody>
				@if($table_data)
					@foreach($table_data as $row)
						<tr>
							@foreach($table_header as $k => $v)
								<td>{{ $row->$v }}</td>
							@endforeach
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="15">No Record Found</td>
					</tr>
				@endif
			</tbody>
	@endif
</table>
@if($table_data)
	<div class="dataTables_paginate paging_simple_numbers">
		{!! $table_data->render() !!}
	</div>
@endif

<script>
	jQuery('.pagination a').on('click', function(e){
		jQuery('#ajax').modal('show');
		e.preventDefault();
		var url = $(this).attr('href');
		var list_name = jQuery('input[name=table_name]').val();
		var csrf = "{!! session()->get('_token') !!}";

		jQuery.ajax({
			url: url,
			type: 'POST',
			data: {page_link: url, table_name: list_name, _token: csrf},
			success: function(response) {
				jQuery('#result-container').html(response);
				jQuery('#ajax').modal('hide');
			},
			error: function() {}
		})
	});
</script>