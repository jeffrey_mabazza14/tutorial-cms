@extends('layouts.default')
@section('styles')
	<link href="{{ asset('assets/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
	<style>
		.error {
			color: red !important;
			font-style: italic;
		}
	</style>
@stop

@section('content')
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="portlet box yellow-lemon">
				<!-- BEGIN PAGE HEADER-->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<span>User</span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa icon-briefcase"></i>
							<span>Change Password</span>
							<i class="fa fa-angle-right"></i>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-plus"></i>User - Change Password
					</div>
				</div>
				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{!! Form::open(['url'=>url('user/change-password/confirm'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'return validateChangePassword()']) !!}
						<input type="hidden" name="id" value="{{ $user_id }}">
						<div class="form-body">
							<div class="alert alert-danger {{ !isset($error_message) ? 'display-hide' : '' }}" id="error">
								<button class="close" data-close="alert"></button>
								<span>
									{{ isset($error_message) ? $error_message : '' }}
								 </span>
							</div>
							@if(isset($success_message))
								<div class="alert alert-success" id="success">
									<button class="close" data-close="alert"></button>
									<span>
										{{ $success_message }}
									 </span>
								</div>
							@endif
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Old Password</label>
										<div class="col-md-9">
											<input type="text" class="form-control" placeholder="" name="old_password" value="{{ isset($row) ? $row->description : '' }}" required>
											<span class="help-block">
											</span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">New Password</label>
										<div class="col-md-9">
											<input type="password" class="form-control" placeholder="" name="password" value="{{ isset($row) ? $row->from_email : '' }}" required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Re-type New Password</label>
										<div class="col-md-9">
											<input type="password" class="form-control" placeholder="" name="repassword" value="{{ isset($row) ? $row->from_email : '' }}" required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn yellow-lemon">Submit</button>
											<a href="{{ URL::previous() }}" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					{!! Form::close() !!}
					<!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/login.js') }}" type="text/javascript"></script>
@stop

@section('defined-scripts')
	<script>
		$('#sender-form').validate();
	</script>

	<script>
		function validateChangePassword() {
			var error = false;
			var error_container = $('#error');
			var new_password = $('input[name=password]').val();
			var re_password = $('input[name=repassword]').val();
			var success_message = $('#success');

			if(new_password != re_password) {
				error = 'New password and old password does not matched.';
			}

			if(error) {
				success_message.hide();
				error_container.show().html(error);
				return false;
			} else {
				error_container.hide();
				return true;
			}
		}
	</script>

	<script>
	jQuery(document).ready(function() {    
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
	   	FormSamples.init();
	   	Login.init();
	});
	</script>
@stop