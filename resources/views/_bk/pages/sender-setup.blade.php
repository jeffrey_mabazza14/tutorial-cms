@extends('layouts.default')
@section('styles')
	<link href="{{ asset('assets/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
	<style>
		.error {
			color: red !important;
			font-style: italic;
		}
	</style>
@stop

@section('content')
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="portlet box yellow-lemon">
				<!-- BEGIN PAGE HEADER-->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<span>Home</span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa icon-briefcase"></i>
							<span>Sender Addresses</span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-plus"></i>
							<span>{{ isset($row) ? 'Update' : 'Create' }}</span>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-plus"></i>Sender Address - {{ isset($row) ? 'Update' : 'Create' }}
					</div>
				</div>
				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{!! Form::open(['url'=>isset($row) ? url('sender/update') : url('sender/save'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form']) !!}
						<input type="hidden" name="id" value="{{ isset($row) ? $row->id : '' }}">
						<div class="form-body">
							<h3 class="form-section">Sender Information</h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Description</label>
										<div class="col-md-9">
											<input type="text" class="form-control" placeholder="" name="description" value="{{ isset($row) ? $row->description : '' }}" required>
											<span class="help-block">
											Enter a descriptive name for this Sender Address. Please note: This field will be used to identify this Sender Address when making API calls. </span>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">From name</label>
										<div class="col-md-9">
											<input type="text" class="form-control" placeholder="" name="from_name" value="{{ isset($row) ? $row->from_name : '' }}" required>
											<span class="help-block"></span>

										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">From email</label>
										<div class="col-md-9">
											<input type="email" class="form-control" placeholder="example@domain.com" name="from_email" value="{{ isset($row) ? $row->from_email : '' }}" required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Reply-to email</label>
										<div class="col-md-9">
											<input type="email" class="form-control" placeholder="" name="reply_to" value="{{ isset($row) ? $row->reply_to : '' }}" required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<h3 class="form-section">Address</h3>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Address</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="address" value="{{ isset($row) ? $row->address : '' }}" required>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">City</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="city" value="{{ isset($row) ? $row->city : '' }}" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">State</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="state" value="{{ isset($row) ? $row->state : '' }}">
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Zip</label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="zip" value="{{ isset($row) ? $row->zip : '' }}" required>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Country</label>
										<div class="col-md-9">
											<select name="country" class="form-control" required>
												<option value=""></option>
												<option value="AF" {{ isset($row) && $row->country == 'AF' ? 'selected' : '' }}>Afghanistan</option>
												<option value="AL" {{ isset($row) && $row->country == 'AL' ? 'selected' : '' }}>Albania</option>
												<option value="DZ" {{ isset($row) && $row->country == 'DZ' ? 'selected' : '' }}>Algeria</option>
												<option value="AS" {{ isset($row) && $row->country == 'AS' ? 'selected' : '' }}>American Samoa</option>
												<option value="AD" {{ isset($row) && $row->country == 'AD' ? 'selected' : '' }}>Andorra</option>
												<option value="AO" {{ isset($row) && $row->country == 'AO' ? 'selected' : '' }}>Angola</option>
												<option value="AI" {{ isset($row) && $row->country == 'AI' ? 'selected' : '' }}>Anguilla</option>
												<option value="AR" {{ isset($row) && $row->country == 'AR' ? 'selected' : '' }}>Argentina</option>
												<option value="AM" {{ isset($row) && $row->country == 'AM' ? 'selected' : '' }}>Armenia</option>
												<option value="AW" {{ isset($row) && $row->country == 'AW' ? 'selected' : '' }}>Aruba</option>
												<option value="AU" {{ isset($row) && $row->country == 'AU' ? 'selected' : '' }}>Australia</option>
												<option value="AT" {{ isset($row) && $row->country == 'AT' ? 'selected' : '' }}>Austria</option>
												<option value="AZ" {{ isset($row) && $row->country == 'AZ' ? 'selected' : '' }}>Azerbaijan</option>
												<option value="BS" {{ isset($row) && $row->country == 'BS' ? 'selected' : '' }}>Bahamas</option>
												<option value="BH" {{ isset($row) && $row->country == 'BH' ? 'selected' : '' }}>Bahrain</option>
												<option value="BD" {{ isset($row) && $row->country == 'BD' ? 'selected' : '' }}>Bangladesh</option>
												<option value="BB" {{ isset($row) && $row->country == 'BB' ? 'selected' : '' }}>Barbados</option>
												<option value="BY" {{ isset($row) && $row->country == 'BY' ? 'selected' : '' }}>Belarus</option>
												<option value="BE" {{ isset($row) && $row->country == 'BE' ? 'selected' : '' }}>Belgium</option>
												<option value="BZ" {{ isset($row) && $row->country == 'BZ' ? 'selected' : '' }}>Belize</option>
												<option value="BJ" {{ isset($row) && $row->country == 'BJ' ? 'selected' : '' }}>Benin</option>
												<option value="BM" {{ isset($row) && $row->country == 'BM' ? 'selected' : '' }}>Bermuda</option>
												<option value="BT" {{ isset($row) && $row->country == 'BT' ? 'selected' : '' }}>Bhutan</option>
												<option value="BO" {{ isset($row) && $row->country == 'BO' ? 'selected' : '' }}>Bolivia</option>
												<option value="BA" {{ isset($row) && $row->country == 'BA' ? 'selected' : '' }}>Bosnia and Herzegowina</option>
												<option value="BW" {{ isset($row) && $row->country == 'BW' ? 'selected' : '' }}>Botswana</option>
												<option value="BV" {{ isset($row) && $row->country == 'BV' ? 'selected' : '' }}>Bouvet Island</option>
												<option value="BR" {{ isset($row) && $row->country == 'BR' ? 'selected' : '' }}>Brazil</option>
												<option value="IO" {{ isset($row) && $row->country == 'IO' ? 'selected' : '' }}>British Indian Ocean Territory</option>
												<option value="BN" {{ isset($row) && $row->country == 'BN' ? 'selected' : '' }}>Brunei Darussalam</option>
												<option value="BG" {{ isset($row) && $row->country == 'BG' ? 'selected' : '' }}>Bulgaria</option>
												<option value="BF" {{ isset($row) && $row->country == 'BF' ? 'selected' : '' }}>Burkina Faso</option>
												<option value="BI" {{ isset($row) && $row->country == 'BI' ? 'selected' : '' }}>Burundi</option>
												<option value="KH" {{ isset($row) && $row->country == 'KH' ? 'selected' : '' }}>Cambodia</option>
												<option value="CM" {{ isset($row) && $row->country == 'CM' ? 'selected' : '' }}>Cameroon</option>
												<option value="CA" {{ isset($row) && $row->country == 'CA' ? 'selected' : '' }}>Canada</option>
												<option value="CV" {{ isset($row) && $row->country == 'CV' ? 'selected' : '' }}>Cape Verde</option>
												<option value="KY" {{ isset($row) && $row->country == 'KY' ? 'selected' : '' }}>Cayman Islands</option>
												<option value="CF" {{ isset($row) && $row->country == 'CF' ? 'selected' : '' }}>Central African Republic</option>
												<option value="TD" {{ isset($row) && $row->country == 'TD' ? 'selected' : '' }}>Chad</option>
												<option value="CL" {{ isset($row) && $row->country == 'CL' ? 'selected' : '' }}>Chile</option>
												<option value="CN" {{ isset($row) && $row->country == 'CN' ? 'selected' : '' }}>China</option>
												<option value="CX" {{ isset($row) && $row->country == 'CX' ? 'selected' : '' }}>Christmas Island</option>
												<option value="CC" {{ isset($row) && $row->country == 'CC' ? 'selected' : '' }}>Cocos (Keeling) Islands</option>
												<option value="CO" {{ isset($row) && $row->country == 'CO' ? 'selected' : '' }}>Colombia</option>
												<option value="KM" {{ isset($row) && $row->country == 'KM' ? 'selected' : '' }}>Comoros</option>
												<option value="CG" {{ isset($row) && $row->country == 'CG' ? 'selected' : '' }}>Congo</option>
												<option value="CD" {{ isset($row) && $row->country == 'CD' ? 'selected' : '' }}>Congo, the Democratic Republic of the</option>
												<option value="CK" {{ isset($row) && $row->country == 'CK' ? 'selected' : '' }}>Cook Islands</option>
												<option value="CR" {{ isset($row) && $row->country == 'CR' ? 'selected' : '' }}>Costa Rica</option>
												<option value="CI" {{ isset($row) && $row->country == 'CI' ? 'selected' : '' }}>Cote d'Ivoire</option>
												<option value="HR" {{ isset($row) && $row->country == 'HR' ? 'selected' : '' }}>Croatia (Hrvatska)</option>
												<option value="CU" {{ isset($row) && $row->country == 'CU' ? 'selected' : '' }}>Cuba</option>
												<option value="CY" {{ isset($row) && $row->country == 'CY' ? 'selected' : '' }}>Cyprus</option>
												<option value="CZ" {{ isset($row) && $row->country == 'CZ' ? 'selected' : '' }}>Czech Republic</option>
												<option value="DK" {{ isset($row) && $row->country == 'DK' ? 'selected' : '' }}>Denmark</option>
												<option value="DJ" {{ isset($row) && $row->country == 'DJ' ? 'selected' : '' }}>Djibouti</option>
												<option value="DM" {{ isset($row) && $row->country == 'DM' ? 'selected' : '' }}>Dominica</option>
												<option value="DO" {{ isset($row) && $row->country == 'DO' ? 'selected' : '' }}>Dominican Republic</option>
												<option value="EC" {{ isset($row) && $row->country == 'EC' ? 'selected' : '' }}>Ecuador</option>
												<option value="EG" {{ isset($row) && $row->country == 'EG' ? 'selected' : '' }}>Egypt</option>
												<option value="SV" {{ isset($row) && $row->country == 'SV' ? 'selected' : '' }}>El Salvador</option>
												<option value="GQ" {{ isset($row) && $row->country == 'GQ' ? 'selected' : '' }}>Equatorial Guinea</option>
												<option value="ER" {{ isset($row) && $row->country == 'ER' ? 'selected' : '' }}>Eritrea</option>
												<option value="EE" {{ isset($row) && $row->country == 'EE' ? 'selected' : '' }}>Estonia</option>
												<option value="ET" {{ isset($row) && $row->country == 'ET' ? 'selected' : '' }}>Ethiopia</option>
												<option value="FK" {{ isset($row) && $row->country == 'FK' ? 'selected' : '' }}>Falkland Islands (Malvinas)</option>
												<option value="FO" {{ isset($row) && $row->country == 'FO' ? 'selected' : '' }}>Faroe Islands</option>
												<option value="FJ" {{ isset($row) && $row->country == 'FJ' ? 'selected' : '' }}>Fiji</option>
												<option value="FI" {{ isset($row) && $row->country == 'FI' ? 'selected' : '' }}>Finland</option>
												<option value="FR" {{ isset($row) && $row->country == 'FR' ? 'selected' : '' }}>France</option>
												<option value="GF" {{ isset($row) && $row->country == 'GF' ? 'selected' : '' }}>French Guiana</option>
												<option value="PF" {{ isset($row) && $row->country == 'PF' ? 'selected' : '' }}>French Polynesia</option>
												<option value="TF" {{ isset($row) && $row->country == 'TF' ? 'selected' : '' }}>French Southern Territories</option>
												<option value="GA" {{ isset($row) && $row->country == 'GA' ? 'selected' : '' }}>Gabon</option>
												<option value="GM" {{ isset($row) && $row->country == 'GM' ? 'selected' : '' }}>Gambia</option>
												<option value="GE" {{ isset($row) && $row->country == 'GE' ? 'selected' : '' }}>Georgia</option>
												<option value="DE" {{ isset($row) && $row->country == 'DE' ? 'selected' : '' }}>Germany</option>
												<option value="GH" {{ isset($row) && $row->country == 'GH' ? 'selected' : '' }}>Ghana</option>
												<option value="GI" {{ isset($row) && $row->country == 'GI' ? 'selected' : '' }}>Gibraltar</option>
												<option value="GR" {{ isset($row) && $row->country == 'GR' ? 'selected' : '' }}>Greece</option>
												<option value="GL" {{ isset($row) && $row->country == 'GL' ? 'selected' : '' }}>Greenland</option>
												<option value="GD" {{ isset($row) && $row->country == 'GD' ? 'selected' : '' }}>Grenada</option>
												<option value="GP" {{ isset($row) && $row->country == 'GP' ? 'selected' : '' }}>Guadeloupe</option>
												<option value="GU" {{ isset($row) && $row->country == 'GU' ? 'selected' : '' }}>Guam</option>
												<option value="GT" {{ isset($row) && $row->country == 'GT' ? 'selected' : '' }}>Guatemala</option>
												<option value="GN" {{ isset($row) && $row->country == 'GN' ? 'selected' : '' }}>Guinea</option>
												<option value="GW" {{ isset($row) && $row->country == 'GW' ? 'selected' : '' }}>Guinea-Bissau</option>
												<option value="GY" {{ isset($row) && $row->country == 'GY' ? 'selected' : '' }}>Guyana</option>
												<option value="HT" {{ isset($row) && $row->country == 'HT' ? 'selected' : '' }}>Haiti</option>
												<option value="HM" {{ isset($row) && $row->country == 'HM' ? 'selected' : '' }}>Heard and Mc Donald Islands</option>
												<option value="VA" {{ isset($row) && $row->country == 'VA' ? 'selected' : '' }}>Holy See (Vatican City State)</option>
												<option value="HN" {{ isset($row) && $row->country == 'HN' ? 'selected' : '' }}>Honduras</option>
												<option value="HK" {{ isset($row) && $row->country == 'HK' ? 'selected' : '' }}>Hong Kong</option>
												<option value="HU" {{ isset($row) && $row->country == 'HU' ? 'selected' : '' }}>Hungary</option>
												<option value="IS" {{ isset($row) && $row->country == 'IS' ? 'selected' : '' }}>Iceland</option>
												<option value="IN" {{ isset($row) && $row->country == 'IN' ? 'selected' : '' }}>India</option>
												<option value="ID" {{ isset($row) && $row->country == 'ID' ? 'selected' : '' }}>Indonesia</option>
												<option value="IR" {{ isset($row) && $row->country == 'IR' ? 'selected' : '' }}>Iran (Islamic Republic of)</option>
												<option value="IQ" {{ isset($row) && $row->country == 'IQ' ? 'selected' : '' }}>Iraq</option>
												<option value="IE" {{ isset($row) && $row->country == 'IE' ? 'selected' : '' }}>Ireland</option>
												<option value="IL" {{ isset($row) && $row->country == 'IL' ? 'selected' : '' }}>Israel</option>
												<option value="IT" {{ isset($row) && $row->country == 'IT' ? 'selected' : '' }}>Italy</option>
												<option value="JM" {{ isset($row) && $row->country == 'JM' ? 'selected' : '' }}>Jamaica</option>
												<option value="JP" {{ isset($row) && $row->country == 'JP' ? 'selected' : '' }}>Japan</option>
												<option value="JO" {{ isset($row) && $row->country == 'JO' ? 'selected' : '' }}>Jordan</option>
												<option value="KZ" {{ isset($row) && $row->country == 'KZ' ? 'selected' : '' }}>Kazakhstan</option>
												<option value="KE" {{ isset($row) && $row->country == 'KE' ? 'selected' : '' }}>Kenya</option>
												<option value="KI" {{ isset($row) && $row->country == 'KI' ? 'selected' : '' }}>Kiribati</option>
												<option value="KP" {{ isset($row) && $row->country == 'KP' ? 'selected' : '' }}>Korea, Democratic People's Republic of</option>
												<option value="KR" {{ isset($row) && $row->country == 'KR' ? 'selected' : '' }}>Korea, Republic of</option>
												<option value="KW" {{ isset($row) && $row->country == 'KW' ? 'selected' : '' }}>Kuwait</option>
												<option value="KG" {{ isset($row) && $row->country == 'KG' ? 'selected' : '' }}>Kyrgyzstan</option>
												<option value="LA" {{ isset($row) && $row->country == 'LA' ? 'selected' : '' }}>Lao People's Democratic Republic</option>
												<option value="LV" {{ isset($row) && $row->country == 'LV' ? 'selected' : '' }}>Latvia</option>
												<option value="LB" {{ isset($row) && $row->country == 'LB' ? 'selected' : '' }}>Lebanon</option>
												<option value="LS" {{ isset($row) && $row->country == 'LS' ? 'selected' : '' }}>Lesotho</option>
												<option value="LR" {{ isset($row) && $row->country == 'LR' ? 'selected' : '' }}>Liberia</option>
												<option value="LY" {{ isset($row) && $row->country == 'LY' ? 'selected' : '' }}>Libyan Arab Jamahiriya</option>
												<option value="LI" {{ isset($row) && $row->country == 'LI' ? 'selected' : '' }}>Liechtenstein</option>
												<option value="LT" {{ isset($row) && $row->country == 'LT' ? 'selected' : '' }}>Lithuania</option>
												<option value="LU" {{ isset($row) && $row->country == 'LU' ? 'selected' : '' }}>Luxembourg</option>
												<option value="MO" {{ isset($row) && $row->country == 'MO' ? 'selected' : '' }}>Macau</option>
												<option value="MK" {{ isset($row) && $row->country == 'MK' ? 'selected' : '' }}>Macedonia, The Former Yugoslav Republic of</option>
												<option value="MG" {{ isset($row) && $row->country == 'MG' ? 'selected' : '' }}>Madagascar</option>
												<option value="MW" {{ isset($row) && $row->country == 'MW' ? 'selected' : '' }}>Malawi</option>
												<option value="MY" {{ isset($row) && $row->country == 'MY' ? 'selected' : '' }}>Malaysia</option>
												<option value="MV" {{ isset($row) && $row->country == 'MV' ? 'selected' : '' }}>Maldives</option>
												<option value="ML" {{ isset($row) && $row->country == 'ML' ? 'selected' : '' }}>Mali</option>
												<option value="MT" {{ isset($row) && $row->country == 'MT' ? 'selected' : '' }}>Malta</option>
												<option value="MH" {{ isset($row) && $row->country == 'MH' ? 'selected' : '' }}>Marshall Islands</option>
												<option value="MQ" {{ isset($row) && $row->country == 'MQ' ? 'selected' : '' }}>Martinique</option>
												<option value="MR" {{ isset($row) && $row->country == 'MR' ? 'selected' : '' }}>Mauritania</option>
												<option value="MU" {{ isset($row) && $row->country == 'MU' ? 'selected' : '' }}>Mauritius</option>
												<option value="YT" {{ isset($row) && $row->country == 'YT' ? 'selected' : '' }}>Mayotte</option>
												<option value="MX" {{ isset($row) && $row->country == 'MX' ? 'selected' : '' }}>Mexico</option>
												<option value="FM" {{ isset($row) && $row->country == 'FM' ? 'selected' : '' }}>Micronesia, Federated States of</option>
												<option value="MD" {{ isset($row) && $row->country == 'MD' ? 'selected' : '' }}>Moldova, Republic of</option>
												<option value="MC" {{ isset($row) && $row->country == 'MC' ? 'selected' : '' }}>Monaco</option>
												<option value="MN" {{ isset($row) && $row->country == 'MN' ? 'selected' : '' }}>Mongolia</option>
												<option value="MS" {{ isset($row) && $row->country == 'MS' ? 'selected' : '' }}>Montserrat</option>
												<option value="MA" {{ isset($row) && $row->country == 'MA' ? 'selected' : '' }}>Morocco</option>
												<option value="MZ" {{ isset($row) && $row->country == 'MZ' ? 'selected' : '' }}>Mozambique</option>
												<option value="MM" {{ isset($row) && $row->country == 'MM' ? 'selected' : '' }}>Myanmar</option>
												<option value="NA" {{ isset($row) && $row->country == 'NA' ? 'selected' : '' }}>Namibia</option>
												<option value="NR" {{ isset($row) && $row->country == 'NR' ? 'selected' : '' }}>Nauru</option>
												<option value="NP" {{ isset($row) && $row->country == 'NP' ? 'selected' : '' }}>Nepal</option>
												<option value="NL" {{ isset($row) && $row->country == 'NL' ? 'selected' : '' }}>Netherlands</option>
												<option value="AN" {{ isset($row) && $row->country == 'AN' ? 'selected' : '' }}>Netherlands Antilles</option>
												<option value="NC" {{ isset($row) && $row->country == 'NC' ? 'selected' : '' }}>New Caledonia</option>
												<option value="NZ" {{ isset($row) && $row->country == 'NZ' ? 'selected' : '' }}>New Zealand</option>
												<option value="NI" {{ isset($row) && $row->country == 'NI' ? 'selected' : '' }}>Nicaragua</option>
												<option value="NE" {{ isset($row) && $row->country == 'NE' ? 'selected' : '' }}>Niger</option>
												<option value="NG" {{ isset($row) && $row->country == 'NG' ? 'selected' : '' }}>Nigeria</option>
												<option value="NU" {{ isset($row) && $row->country == 'NU' ? 'selected' : '' }}>Niue</option>
												<option value="NF" {{ isset($row) && $row->country == 'NF' ? 'selected' : '' }}>Norfolk Island</option>
												<option value="MP" {{ isset($row) && $row->country == 'MP' ? 'selected' : '' }}>Northern Mariana Islands</option>
												<option value="NO" {{ isset($row) && $row->country == 'NO' ? 'selected' : '' }}>Norway</option>
												<option value="OM" {{ isset($row) && $row->country == 'OM' ? 'selected' : '' }}>Oman</option>
												<option value="PK" {{ isset($row) && $row->country == 'PK' ? 'selected' : '' }}>Pakistan</option>
												<option value="PW" {{ isset($row) && $row->country == 'PW' ? 'selected' : '' }}>Palau</option>
												<option value="PA" {{ isset($row) && $row->country == 'PA' ? 'selected' : '' }}>Panama</option>
												<option value="PG" {{ isset($row) && $row->country == 'PG' ? 'selected' : '' }}>Papua New Guinea</option>
												<option value="PY" {{ isset($row) && $row->country == 'PY' ? 'selected' : '' }}>Paraguay</option>
												<option value="PE" {{ isset($row) && $row->country == 'PE' ? 'selected' : '' }}>Peru</option>
												<option value="PH" selected {{ isset($row) && $row->country == 'PH' ? 'selected' : '' }}>Philippines</option>
												<option value="PN" {{ isset($row) && $row->country == 'PN' ? 'selected' : '' }}>Pitcairn</option>
												<option value="PL" {{ isset($row) && $row->country == 'PL' ? 'selected' : '' }}>Poland</option>
												<option value="PT" {{ isset($row) && $row->country == 'PT' ? 'selected' : '' }}>Portugal</option>
												<option value="PR" {{ isset($row) && $row->country == 'PR' ? 'selected' : '' }}>Puerto Rico</option>
												<option value="QA" {{ isset($row) && $row->country == 'QA' ? 'selected' : '' }}>Qatar</option>
												<option value="RE" {{ isset($row) && $row->country == 'RE' ? 'selected' : '' }}>Reunion</option>
												<option value="RO" {{ isset($row) && $row->country == 'RO' ? 'selected' : '' }}>Romania</option>
												<option value="RU" {{ isset($row) && $row->country == 'RU' ? 'selected' : '' }}>Russian Federation</option>
												<option value="RW" {{ isset($row) && $row->country == 'RW' ? 'selected' : '' }}>Rwanda</option>
												<option value="KN" {{ isset($row) && $row->country == 'KN' ? 'selected' : '' }}>Saint Kitts and Nevis</option>
												<option value="LC" {{ isset($row) && $row->country == 'LC' ? 'selected' : '' }}>Saint LUCIA</option>
												<option value="VC" {{ isset($row) && $row->country == 'VC' ? 'selected' : '' }}>Saint Vincent and the Grenadines</option>
												<option value="WS" {{ isset($row) && $row->country == 'WS' ? 'selected' : '' }}>Samoa</option>
												<option value="SM" {{ isset($row) && $row->country == 'SM' ? 'selected' : '' }}>San Marino</option>
												<option value="ST" {{ isset($row) && $row->country == 'ST' ? 'selected' : '' }}>Sao Tome and Principe</option>
												<option value="SA" {{ isset($row) && $row->country == 'SA' ? 'selected' : '' }}>Saudi Arabia</option>
												<option value="SN" {{ isset($row) && $row->country == 'SN' ? 'selected' : '' }}>Senegal</option>
												<option value="SC" {{ isset($row) && $row->country == 'SC' ? 'selected' : '' }}>Seychelles</option>
												<option value="SL" {{ isset($row) && $row->country == 'SL' ? 'selected' : '' }}>Sierra Leone</option>
												<option value="SG" {{ isset($row) && $row->country == 'SG' ? 'selected' : '' }}>Singapore</option>
												<option value="SK" {{ isset($row) && $row->country == 'SK' ? 'selected' : '' }}>Slovakia (Slovak Republic)</option>
												<option value="SI" {{ isset($row) && $row->country == 'SI' ? 'selected' : '' }}>Slovenia</option>
												<option value="SB" {{ isset($row) && $row->country == 'SB' ? 'selected' : '' }}>Solomon Islands</option>
												<option value="SO" {{ isset($row) && $row->country == 'SO' ? 'selected' : '' }}>Somalia</option>
												<option value="ZA" {{ isset($row) && $row->country == 'ZA' ? 'selected' : '' }}>South Africa</option>
												<option value="GS" {{ isset($row) && $row->country == 'GS' ? 'selected' : '' }}>South Georgia and the South Sandwich Islands</option>
												<option value="ES" {{ isset($row) && $row->country == 'ES' ? 'selected' : '' }}>Spain</option>
												<option value="LK" {{ isset($row) && $row->country == 'LK' ? 'selected' : '' }}>Sri Lanka</option>
												<option value="SH" {{ isset($row) && $row->country == 'SH' ? 'selected' : '' }}>St. Helena</option>
												<option value="PM" {{ isset($row) && $row->country == 'PM' ? 'selected' : '' }}>St. Pierre and Miquelon</option>
												<option value="SD" {{ isset($row) && $row->country == 'SD' ? 'selected' : '' }}>Sudan</option>
												<option value="SR" {{ isset($row) && $row->country == 'SR' ? 'selected' : '' }}>Suriname</option>
												<option value="SJ" {{ isset($row) && $row->country == 'SJ' ? 'selected' : '' }}>Svalbard and Jan Mayen Islands</option>
												<option value="SZ" {{ isset($row) && $row->country == 'SZ' ? 'selected' : '' }}>Swaziland</option>
												<option value="SE" {{ isset($row) && $row->country == 'SE' ? 'selected' : '' }}>Sweden</option>
												<option value="CH" {{ isset($row) && $row->country == 'CH' ? 'selected' : '' }}>Switzerland</option>
												<option value="SY" {{ isset($row) && $row->country == 'SY' ? 'selected' : '' }}>Syrian Arab Republic</option>
												<option value="TW" {{ isset($row) && $row->country == 'TW' ? 'selected' : '' }}>Taiwan, Province of China</option>
												<option value="TJ" {{ isset($row) && $row->country == 'TJ' ? 'selected' : '' }}>Tajikistan</option>
												<option value="TZ" {{ isset($row) && $row->country == 'TZ' ? 'selected' : '' }}>Tanzania, United Republic of</option>
												<option value="TH" {{ isset($row) && $row->country == 'TH' ? 'selected' : '' }}>Thailand</option>
												<option value="TG" {{ isset($row) && $row->country == 'TG' ? 'selected' : '' }}>Togo</option>
												<option value="TK" {{ isset($row) && $row->country == 'TK' ? 'selected' : '' }}>Tokelau</option>
												<option value="TO" {{ isset($row) && $row->country == 'TO' ? 'selected' : '' }}>Tonga</option>
												<option value="TT" {{ isset($row) && $row->country == 'TT' ? 'selected' : '' }}>Trinidad and Tobago</option>
												<option value="TN" {{ isset($row) && $row->country == 'TN' ? 'selected' : '' }}>Tunisia</option>
												<option value="TR" {{ isset($row) && $row->country == 'TR' ? 'selected' : '' }}>Turkey</option>
												<option value="TM" {{ isset($row) && $row->country == 'TM' ? 'selected' : '' }}>Turkmenistan</option>
												<option value="TC" {{ isset($row) && $row->country == 'TC' ? 'selected' : '' }}>Turks and Caicos Islands</option>
												<option value="TV" {{ isset($row) && $row->country == 'TV' ? 'selected' : '' }}>Tuvalu</option>
												<option value="UG" {{ isset($row) && $row->country == 'UG' ? 'selected' : '' }}>Uganda</option>
												<option value="UA" {{ isset($row) && $row->country == 'UA' ? 'selected' : '' }}>Ukraine</option>
												<option value="AE" {{ isset($row) && $row->country == 'AE' ? 'selected' : '' }}>United Arab Emirates</option>
												<option value="GB" {{ isset($row) && $row->country == 'GB' ? 'selected' : '' }}>United Kingdom</option>
												<option value="US" {{ isset($row) && $row->country == 'US' ? 'selected' : '' }}>United States</option>
												<option value="UM" {{ isset($row) && $row->country == 'UM' ? 'selected' : '' }}>United States Minor Outlying Islands</option>
												<option value="UY" {{ isset($row) && $row->country == 'UY' ? 'selected' : '' }}>Uruguay</option>
												<option value="UZ" {{ isset($row) && $row->country == 'UZ' ? 'selected' : '' }}>Uzbekistan</option>
												<option value="VU" {{ isset($row) && $row->country == 'VU' ? 'selected' : '' }}>Vanuatu</option>
												<option value="VE" {{ isset($row) && $row->country == 'VE' ? 'selected' : '' }}>Venezuela</option>
												<option value="VN" {{ isset($row) && $row->country == 'VN' ? 'selected' : '' }}>Viet Nam</option>
												<option value="VG" {{ isset($row) && $row->country == 'VG' ? 'selected' : '' }}>Virgin Islands (British)</option>
												<option value="VI" {{ isset($row) && $row->country == 'VI' ? 'selected' : '' }}>Virgin Islands (U.S.)</option>
												<option value="WF" {{ isset($row) && $row->country == 'WF' ? 'selected' : '' }}>Wallis and Futuna Islands</option>
												<option value="EH" {{ isset($row) && $row->country == 'EH' ? 'selected' : '' }}>Western Sahara</option>
												<option value="YE" {{ isset($row) && $row->country == 'YE' ? 'selected' : '' }}>Yemen</option>
												<option value="ZM" {{ isset($row) && $row->country == 'ZM' ? 'selected' : '' }}>Zambia</option>
												<option value="ZW" {{ isset($row) && $row->country == 'ZW' ? 'selected' : '' }}>Zimbabwe</option>
											</select>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn yellow-lemon">Submit</button>
											<a href="{{ URL::previous() }}" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					{!! Form::close() !!}
					<!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.js') }}" type="text/javascript"></script>
@stop

@section('defined-scripts')
	<script>
		$('#sender-form').validate();
	</script>

	<script>
	jQuery(document).ready(function() {    
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
	   	FormSamples.init();
	});
	</script>
@stop