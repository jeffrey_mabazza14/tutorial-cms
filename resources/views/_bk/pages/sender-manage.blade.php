@extends('layouts.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/select2/select2.css') }}"/>
@stop

@section('content')
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Home</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa icon-briefcase"></i>
						<span>Sender Addresses</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-envelope-o"></i>
						<span>Manage</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box yellow-lemon">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-envelope-o"></i>Sender Addresses <span class="badge badge-alert">{{ $total }} </span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-bordered">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 Description
									</th>
									<th>
										 From Name
									</th>
									<th>
										 From Email
									</th>
									<th>
										 Reply-to Email
									</th>
									<th>
										 Address
									</th>
									<th>
										 Action
									</th>
								</tr>
								</thead>
								<tbody>
								@if($rows)
									<?php $i = 1; ?>
									@foreach($rows as $row)
										<tr>
											<td>{{ $i }}</td>
											<td>{{ $row->description }}</td>
											<td>{{ $row->from_name }}</td>
											<td>{{ $row->from_email }}</td>
											<td>{{ $row->reply_to }}</td>
											<td>{{ $row->address }}</td>
											<td>
												<a class="btn default btn-xs green" href="#confirm-{{ $row->id }}" role="button" data-toggle="modal">
												<i class="fa fa-edit"></i> Edit </a>
												@if(session('user')['id'] == 1)
													<a href="#delete-{{ $row->id }}" class="btn default btn-xs red" role="button" data-toggle="modal">
													<i class="fa fa-trash-o"></i> Remove </a>
												@endif
											</td>
										</tr>

										{{-- Confirmation Modal --}}
										<div id="confirm-{{ $row->id }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Would you like to continue editing this item?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														<a class="btn green" href="{{ url('sender/edit') . '/' . $row->id }}">Continue</a>
													</div>
												</div>
											</div>
										</div>

										<div id="delete-{{ $row->id }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Would you like to continue deleting this item?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														<a class="btn green" href="{{ url('sender/delete') . '/' . $row->id }}">Continue</a>
													</div>
												</div>
											</div>
										</div>
										{{-- Confirmation Modal --}}
										<?php $i++; ?>
									@endforeach
								@else
									<tr>
										<td colspan="10" style="text-align: center">No Record(s) Found.</td>
									</tr>
								@endif
								
								</tbody>
								</table>
								{{-- Pagination Links --}}
								@if($rows)
									<div class="dataTables_paginate paging_simple_numbers">
										{!! $rows->render() !!}
									</div>
								@endif
								{{-- Pagination Links --}}
							</div>
						</div>
					</div>
				</div>
			</div>

			

			<!-- END PAGE CONTENT-->
		</div>
	</div>
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
@stop
@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
@stop
@section('defined-scripts')
	<script>
	jQuery(document).ready(function() {       
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
	});
	</script>
@stop