@extends('layouts.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>
@endsection
@section('content')
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Home</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-tachometer"></i>
						<span>Dashboard</span>
					</li>
				</ul>
			</div>
			<br />
			<!-- END PAGE HEADER-->
			{{-- BEGIN ACTIVITIES --}}
			{{-- <div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-share font-blue-steel hide"></i>
								<span class="caption-subject font-blue-steel bold uppercase">Recent Activities</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
								<ul class="feeds">
									@if($audits)
										@foreach($audits as $audit)
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm {{ stripos($audit->activity, 'fail') !== FALSE ? 'label-danger' : 'label-success' }}">
																<i class="fa fa-user"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 {{ $audit->activity }}
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date" title="{{ date('M d, Y h:i:s', strtotime($audit->created_at)) }}">
														 {{ $audit->time_ago }}
													</div>
												</div>
											</li>
										@endforeach
									@endif
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div> --}}
			{{-- END ACTIVITIES --}}
			<div class="clearfix"></div>
			{{-- STATS --}}
			<div class="portlet-title" style="margin: 30px 30px 15px 30px !important">
				<div class="caption">
					<i class="icon-cursor font-purple-intense hide"></i>
					<span class="caption-subject bold uppercase" style="font-size: 19px !important">Overall Metrics</span>
					<br>
					<br>
					<a style="color: #333333" href="{{ url('metric/total') }}"><span class="caption-subject bold uppercase" style="font-size: 17px !important">Total Email Sent : {{ $overall_recipients }}</span></a>
				</div>
			</div>
			{{-- RADIAL POSITIVE STATUS --}}
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="portlet light ">
						<div class="portlet-body">
							<div class="row">
								@foreach($overall_metrics_up_percentage as $k => $v)
									<div class="col-md-2">
										<div class="easy-pie-chart">
											<div class="number transactions" data-percent="{{ $v }}">
												<span>
												{{ $v }} </span>
												%
											</div>
											<a class="title" href="{{ url('metric/') . '/' . $k }}">
												{{ ucwords($k) }}
											</a>
										</div>
									</div>
									<div class="margin-bottom-10 visible-sm">
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
			{{-- RADIAL POSITIVE STATUS --}}

			<div class="row">				
				<div class="col-md-12 col-sm-12">
				{{-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat green">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">
								 {{ $overall_recipients }}
							</div>
							<div class="desc">
								 Total Recipients
							</div>
						</div>
					</div>
				</div> --}}
				@foreach($overall_metrics_up as $k => $v)
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
						<div class="dashboard-stat green">
							<div class="visual">
								<i class="fa fa-bar-chart-o"></i>
							</div>
							<div class="details">
								<div class="number">
									 {{ $v }}
								</div>
								<div class="desc">
									 {{ ucwords($k) }}
								</div>
							</div>
						</div>
					</div>
				@endforeach
				{{-- @foreach($overall_metrics as $k => $v)
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="dashboard-stat {{ in_array($v['event'], $positive) ? 'green' : 'red-thunderbird' }}">
							<div class="visual">
								<i class="fa fa-bar-chart-o"></i>
							</div>
							<div class="details">
								<div class="number">
									 {{ $v['total'] }}
								</div>
								<div class="desc">
									 {{ ucwords($v['event']) }}
								</div>
							</div>
						</div>
					</div>
				@endforeach--}}
			</div>
			<div class="clearfix"></div>			
		</div>

		{{-- RADIAL NEGATIVE STATUS --}}
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="portlet light ">
						<div class="portlet-body">
							<div class="row">
								@foreach($overall_metrics_down_percentage as $k => $v)
									<div class="col-md-2">
										<div class="easy-pie-chart">
											<div class="number transactions" data-percent="{{ $v }}">
												<span>
												{{ $v }} </span>
												%
											</div>
											<a class="title" href="{{ url('metric/') . '/' . $k }}">
											{{ ucwords($k) }}
											</a>
										</div>
									</div>
									<div class="margin-bottom-10 visible-sm">
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
			{{-- RADIAL NEGATIVE STATUS --}}

			<div class="row">				
				<div class="col-md-12 col-sm-12">
				{{-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat green">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">
								 {{ $overall_recipients }}
							</div>
							<div class="desc">
								 Total Recipients
							</div>
						</div>
					</div>
				</div> --}}
				@foreach($overall_metrics_down as $k => $v)
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
						<div class="dashboard-stat red-thunderbird">
							<div class="visual">
								<i class="fa fa-bar-chart-o"></i>
							</div>
							<div class="details">
								<div class="number">
									 {{ $v }}
								</div>
								<div class="desc">
									 {{ ucwords($k) }}
								</div>
							</div>
						</div>
					</div>
				@endforeach
				{{-- @foreach($overall_metrics as $k => $v)
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="dashboard-stat {{ in_array($v['event'], $positive) ? 'green' : 'red-thunderbird' }}">
							<div class="visual">
								<i class="fa fa-bar-chart-o"></i>
							</div>
							<div class="details">
								<div class="number">
									 {{ $v['total'] }}
								</div>
								<div class="desc">
									 {{ ucwords($v['event']) }}
								</div>
							</div>
						</div>
					</div>
				@endforeach--}}
			</div>
			<div class="clearfix"></div>			
		</div>
	</div>
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>
@stop
@section('defined-scripts')
	<script>
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		// ChartsAmcharts.init(); // init demo charts

		Index.init();
		// Index.initDashboardDaterange();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		Index.initMiniCharts();
		// Tasks.initDashboardWidget();
	</script>

@stop
