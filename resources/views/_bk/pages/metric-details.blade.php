@extends('layouts.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/icheck/skins/all.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/select2/select2.css') }}"/>
@stop

@section('content')
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-tachometer"></i>
						<span>Metric Information</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box yellow-lemon">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-tachometer"></i>Metric Information <span class="badge badge-alert">{{ @$total }} </span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-bordered">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 Email
									</th>
									<th>
										 Campaign
									</th>
								</tr>
								</thead>
								<tbody>
								@if($results)
									<?php $offset = isset($_GET['page']) && $_GET['page'] && $_GET['page'] != 1 ? $_GET['page'] * 15 - 15 : NULL ?>
									@foreach($results as $result)
										<tr id="row-{{ $offset }}">
											<td>
												<?php $offset = ($offset === NULL ? 0 : $offset) + 1 ?>
												{{ $offset }}
											</td>
											{{-- <td>{{ $user->username }}</td> --}}
											<td>{{ $result->email }}</td>
											<td>{{ $result->title }}</td>
											{{-- <td>
												@if($user->status == 0)
													<span class="label label-sm label-info">
													Pending </span>
												@elseif($user->status == 1)
													<span class="label label-sm label-success">
													Approved </span>
												@elseif($user->status == 2)
													<span class="label label-sm label-danger">
													Rejected </span>
												@elseif($user->status == 3)
													<span class="label label-sm label-danger">
													Duplicate Email </span>
												@endif
											</td> --}}
											{{-- <td>{{ $user->created_at }}</td> --}}
											{{-- <td>
												<a class="btn default btn-xs red" href="#remove-{{ $offset }}" role="button" data-toggle="modal">
												<i class="fa fa-thumbs-up"></i> Remove </a>
											</td> --}}
											{{-- <td>
												@if($user->status != 1 && $user->status != 3)
													<a class="btn default btn-xs green" href="#approve-{{ $offset }}" role="button" data-toggle="modal">
													<i class="fa fa-thumbs-up"></i> Approve </a>
												@endif
												@if($user->status != 2)
												<a href="#reject-{{ $offset }}" class="btn default btn-xs red" role="button" data-toggle="modal">
												<i class="fa fa-thumbs-down"></i> Reject </a>
												@endif
											</td> --}}
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="10" style="text-align: center">No Record(s) Found.</td>
									</tr>
								@endif
								
								</tbody>
								</table>
								{{-- Pagination Links --}}
								@if($results)
									<div class="dataTables_paginate paging_simple_numbers">
										{!! $results->render() !!}
									</div>
								@endif
								{{-- Pagination Links --}}
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
@stop
@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
@stop
@section('defined-scripts')
	<script>
	jQuery(document).ready(function() {       
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
	});
	</script>
@stop