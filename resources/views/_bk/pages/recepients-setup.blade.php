@extends('layouts.default')
@section('styles')
	<link href="{{ asset('assets/global/plugins/icheck/skins/all.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Home</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa icon-briefcase"></i>
						<span>Recipients</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-plus"></i>
						<span>{{ isset($table_details) && $table_details ? 'Update' : 'Create' }}</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box yellow-lemon" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-plus"></i> Recipients - <span class="step-title">
								{{ isset($table_details) && $table_details ? 'Update' : 'Create' }} </span>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="#" class="form-horizontal" id="submit_form" method="POST">
								<input type="hidden" name="update" value="{{ isset($table_details) && $table_details ? $table_details->id : '' }}">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#create-a-list-name" data-toggle="tab" class="step" id="first-step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Create A List Name </span>
												</a>
											</li>
											<li>
												<a href="#add-recipients" data-toggle="tab" class="step">
												<span class="number">
												2 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Add Recipients </span>
												</a>
											</li>
											<li>
												<a href="#tab3" data-toggle="tab" class="step">
												<span class="number">
												3 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Verify & Save </span>
												</a>
											</li>
										</ul>
										<div class="tab-content">
											{{-- <div class="alert alert-danger display-none">
												<button class="close" data-dismiss="alert"></button>
												Fields with (*) are required.
											</div> --}}
											<div class="alert alert-success display-none">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div>
											<div class="tab-pane active" id="create-a-list-name">
												<h3 class="block">Provide a List Name</h3>
												<div class="form-group">
													<label class="control-label col-md-3" id="label-list-name">List Name <span class="required">
													* </span>
													</label>
													<div class="col-md-6">
														<input type="text" class="form-control" name="list_name" value="{{ isset($table) ? $table : '' }}" required />
														{{-- <span class="help-block">
														Provide your username </span> --}}
													</div>
												</div>
											</div>
											<div class="tab-pane" id="add-recipients">
												<h3 class="block">Add Market Recipients</h3>
												<div class="form-group">
													<label class="control-label col-md-3">Add Option <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<div class="icheck-inline radio-list">
															<label>
															<input required type="radio" name="add_option" value="upload" data-title="Male" onclick="addOptionChange('upload')" {{ isset($table_details) && $table_details->type == 'upload' ? 'checked' : '' }} />
															Upload </label>
															<label>
															<input required type="radio" name="add_option" value="manual" data-title="Female" onclick="addOptionChange('manual')" {{ isset($table_details) && $table_details->type == 'manual' ? 'checked' : '' }} />
															Manual </label>
														</div>
														<div id="form_gender_error">
														</div>
													</div>
												</div>
												<div class="form-group" style="{{ isset($table_details) && $table_details->type == 'upload' ? '' : 'display: none' }}" id="upload">
													<label class="control-label col-md-3">
													</label>
													<div class="col-md-6">
														<div class="col-lg-7">
															<span class="btn green fileinput-button">
																<i class="fa fa-plus"></i>
																<span>
																Select File </span>
																<input type="file" name="csvfile" accept=".csv" onchange="csvChange(this)" id="csv-file">
															</span>
															<span class="help-block" id="upload-text-container">
																{{ isset($table_details) && $table_details->filename !== NULL ? $table_details->filename : 'CSV file only' }}
															</span>
														<div class="alert alert-danger">
															<button class="close" data-dismiss="alert"></button>
															Fields with (*) are required.
														</div>
														</div>
													</div>
												</div>
												<div id="manual" style="{{ isset($table_details) && $table_details->type == 'manual' ? '' : 'display: none' }}">
													@if(isset($table_data) && (count($table_data) > 0))
														<form id="qwerty" method="POST" action="#">
															<div class="form-group">
																<label class="control-label col-md-3">Header </label>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_emails[]" value="email_address" readonly />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_1[]" value="{{ isset($table_header[1]) ? $table_header[1] : '' }}" placeholder="Column 1" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_2[]" value="{{ isset($table_header[2]) ? $table_header[2] : '' }}" placeholder="Column 2" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_3[]" value="{{ isset($table_header[3]) ? $table_header[3] : '' }}" placeholder="Column 3" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_4[]" value="{{ isset($table_header[4]) ? $table_header[4] : '' }}" placeholder="Column 4" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_5[]" value="{{ isset($table_header[5]) ? $table_header[5] : '' }}" placeholder="Column 5" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_6[]" value="{{ isset($table_header[6]) ? $table_header[6] : '' }}" placeholder="Column 6" />
																</div>
															</div>
																<?php $i = 0 ?>
																@foreach($table_data as $k => $v)
																	<?php $rand_del = rand(); ?>
																	<div class="form-group" id="{{ $i === 0 ? 'primary-row' : $rand_del }}" style="margin-bottom: 15px">
																		<label class="control-label col-md-3">
																			@if($i === 0)
																				<button type="button" class="btn btn-xs grey-cascade" onclick="addRow()"><i class="fa fa-plus"></i> Add Row</button>
																			@else
																				<a><i class="glyphicon glyphicon-remove" onclick="removeRow({{ $rand_del }})"></i></a>
																			@endif
																		</label>
																			<div class="col-md-1">
																				<input type="text" class="form-control col-md-2" name="array_emails[]" value="{{ @$v->$table_header[0] }}" />
																			</div>
																			<div class="col-md-1">
																				<input type="text" class="form-control col-md-2" name="array_custom_1[]" value="{{ isset($table_header[1]) ? $v->$table_header[1] : '' }}" />
																			</div>
																			<div class="col-md-1">
																				<input type="text" class="form-control col-md-2" name="array_custom_2[]" value="{{ isset($table_header[2]) ? $v->$table_header[2] : '' }}" />
																			</div>
																			<div class="col-md-1">
																				<input type="text" class="form-control col-md-2" name="array_custom_3[]" value="{{ isset($table_header[3]) ? $v->$table_header[3] : '' }}" />
																			</div>
																			<div class="col-md-1">
																				<input type="text" class="form-control col-md-2" name="array_custom_4[]" value="{{ isset($table_header[4]) ? $v->$table_header[4] : '' }}" />
																			</div>
																			<div class="col-md-1">
																				<input type="text" class="form-control col-md-2" name="array_custom_5[]" value="{{ isset($table_header[5]) ? $v->$table_header[5] : '' }}" />
																			</div>
																			<div class="col-md-1">
																				<input type="text" class="form-control col-md-2" name="array_custom_6[]" value="{{ isset($table_header[6]) ? $v->$table_header[6] : '' }}" />
																			</div>
																	</div>
																	<?php $i++; ?>
																@endforeach
														</form>
													@else
														<form id="qwerty" method="POST" action="#">
															<div class="form-group">
																<label class="control-label col-md-3">Header </label>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_emails[]" value="email_address" readonly />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_1[]" value="" placeholder="Column 1" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_2[]" value="" placeholder="Column 2" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_3[]" value="" placeholder="Column 3" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_4[]" value="" placeholder="Column 4" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_5[]" value="" placeholder="Column 5" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_6[]" value="" placeholder="Column 6" />
																</div>
															</div>
															<div class="form-group" id="primary-row" style="margin-bottom: 15px">
																<label class="control-label col-md-3"><button type="button" class="btn btn-xs grey-cascade" onclick="addRow()"><i class="fa fa-plus"></i> Add Row</button> </label>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_emails[]" value="" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_1[]" value="" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_2[]" value="" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_3[]" value="" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_4[]" value="" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_5[]" value="" />
																</div>
																<div class="col-md-1">
																	<input type="text" class="form-control col-md-2" name="array_custom_6[]" value="" />
																</div>
															</div>
														</form>
													@endif


													
												</div>
											</div>
											<div class="tab-pane" id="tab3">
													<h3 class="block">Verify & Save</h3>
													<div class="portlet-body" id="result-container">
														@if(isset($table_header))
														<table class="table table-bordered table-condensed flip-content">
															<input name="table_name" value="{{ $table_name }}" type="hidden">
															<thead class="flip-content">
																<tr>
																	@foreach($table_header as $k => $v)
																		<th>{{ ucwords(str_replace('_', ' ', $v)) }}</th>
																	@endforeach
																</tr>
																</thead>
																<tbody>
																	@if($table_data)
																		@foreach($table_data as $row)
																			<tr>
																				@foreach($table_header as $k => $v)
																					<td>{{ $row->$v }}</td>
																				@endforeach
																			</tr>
																		@endforeach
																	@else
																		<tr>
																			<td colspan="15">No Record Found</td>
																		</tr>
																	@endif
																</tbody>
															</table>
															@if($table_data)
																<div class="dataTables_paginate paging_simple_numbers">
																	{!! $table_data->render() !!}
																</div>
															@endif
														@else
															<table class="table table-bordered table-condensed flip-content">
															<thead class="flip-content">
															<tr>
																
															</tr>
															</thead>
															<tbody>
															<tr>
																
															</tr>
															</tbody>
															</table>
														@endif
														
													</div>
											</div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">                                                 
												<a href="javascript:;" class="btn default button-previous">
												<i class="m-icon-swapleft"></i> Back </a>
												<a href="javascript:;" class="btn blue button-next" id="verify-continue">
												Continue <i class="m-icon-swapright m-icon-white"></i>
												</a>
												<a href="javascript:;" class="btn yellow-lemon button-submit">
												Save <i class="m-icon-swapright m-icon-white"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
		<!-- START MODALS -->
			<!-- START AJAX MODAL -->
				<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<img src="../../assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
								<span>
								&nbsp;&nbsp;Loading... </span>
							</div>
						</div>
					</div>
				</div>
			<!-- END AJAX MODAL -->
		<!-- END MODALS -->
	<!-- END CONTENT -->
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/icheck/icheck.min.js') }}" type="text/javascript"></script>
	{{-- {!! Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') !!} --}}
@stop
@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script>
		var validate_lists = <?= $recipients ?>;
	</script>
	<script src="{{ asset('assets/admin/pages/scripts/custom-form-wizard.js') }}" type="text/javascript"></script>
	<script>
		function addOptionChange(method) {
			var hasCSV = "{{ isset($table_details) && $table_details->filename !== NULL ? $table_details->filename : 'CSV file only' }}";
			switch(method) {
				case 'upload':
					jQuery('#manual').hide().html('<form id="qwerty" method="POST" action="#"><div class="form-group"><label class="control-label col-md-3">Header </label><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_emails[]" value="email_address" readonly/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_1[]" value="" placeholder="Column 1"/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_2[]" value="" placeholder="Column 2"/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_3[]" value="" placeholder="Column 3"/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_4[]" value="" placeholder="Column 4"/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_5[]" value="" placeholder="Column 5"/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_6[]" value="" placeholder="Column 6"/></div><div class="form-group" id="primary-row" style="margin-bottom: 15px"><label class="control-label col-md-3"><button type="button" class="btn btn-xs grey-cascade" onclick="addRow()"><i class="fa fa-plus"></i> Add Row</button></label><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_emails[]" value=""/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_1[]" value=""/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_2[]" value=""/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_3[]" value=""/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_4[]" value=""/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_5[]" value=""/></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_6[]" value=""/></div></div></form>');
					jQuery('#upload').slideDown(500);
					break;
				case 'manual':
					jQuery('#upload').hide().html('<label class="control-label col-md-3"></label><div class="col-md-6"><div class="col-lg-7"><span class="btn green fileinput-button"><i class="fa fa-plus"></i><span> Select File </span><input type="file" name="csvfile" accept=".csv" onchange="csvChange(this)" id="csv-file"></span><span class="help-block" id="upload-text-container">'+hasCSV+'</span></div></div>');
					jQuery('#manual').slideDown(500);
					break;
				default:
					jQuery('#manual').hide();
					jQuery('#upload').hide();
					break;
			}
		}

		function addRow() {
			var row = Math.floor((Math.random() * 999999) + 1);
			jQuery('#primary-row').after('<div class="form-group" id="'+row+'"><label class="control-label col-md-3"><a><i class="glyphicon glyphicon-remove" onclick="removeRow('+row+')"></i></a></label><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_emails[]" value="" /></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_1[]" value="" /></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_2[]" value="" /></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_3[]" value="" /></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_4[]" value="" /></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_5[]" value="" /></div><div class="col-md-1"><input type="text" class="form-control col-md-1" name="array_custom_6[]" value="" /></div></div>');
		}

		function removeRow(id) {
			jQuery('#'+id).remove();
		}

		function _csvChange() {
			jQuery('#ajax').modal('show');
			var form = new FormData(document.getElementById('submit_form'));
			var file = document.getElementById('csv-file').files[0];
			var csrf = "{!! session()->get('_token') !!}";
			var list_name = jQuery('input[name=list_name]').val();
			if(file) {
				form.append('csv_file', file);
				form.append('_token', "{{ csrf_token() }}");
				form.append('list_name', list_name);
			}
			jQuery.ajax({
				url: "{{ url('recepients/upload') }}",
				type: 'POST',
				data: form,
				contentType: false,
				processData: false,
				beforeSend: function() {},
				success: function(response) {
					jQuery('#result-container').html(response);
				},
				complete: function() {
					jQuery('#ajax').modal('hide');
				},
				error: function() {}
			});
		}

		jQuery('#verify-continue').on('click', function() {
			var list_name = jQuery('input[name=list_name]').val();
			var is_update = jQuery('input[name=update]').val();

			var first_step = $("#first-step");

			if(first_step.attr('aria_expanded')) {
				return false;
			}

			if( ! is_update) {
				if(list_name) {
					var column_1 = jQuery("input[name='array_emails[]']").serializeArray();
					var column_2 = jQuery("input[name='array_custom_1[]']").serializeArray();
					var column_3 = jQuery("input[name='array_custom_2[]']").serializeArray();
					var column_4 = jQuery("input[name='array_custom_3[]']").serializeArray();
					var column_5 = jQuery("input[name='array_custom_4[]']").serializeArray();
					var column_6 = jQuery("input[name='array_custom_5[]']").serializeArray();
					var column_7 = jQuery("input[name='array_custom_6[]']").serializeArray();
					var add_option = jQuery('input[name=add_option]:checked').val();
					var csrf = "{!! session()->get('_token') !!}";
					if(add_option && add_option == 'manual') {
						jQuery.ajax({
							url: "{{ url('recepients/manual') }}",
							type: 'POST',
							data: {
								table_name : list_name,
								column_1 : column_1,
								column_2 : column_2,
								column_3 : column_3,
								column_4 : column_4,
								column_5 : column_5,
								column_6 : column_6,
								column_7 : column_7,
								_token : csrf
							},
							beforeSend: function() {
								jQuery('#ajax').modal('show');
							},
							success: function(response) {
								jQuery('#result-container').html(response);
							},
							complete: function() {
								jQuery('#ajax').modal('hide');
							},
							error: function(){}
						});
					}				
				}
			} else {
				if(jQuery('.form-wizard ul li:nth-child(2)').hasClass('active')) {
					var column_1 = jQuery("input[name='array_emails[]']").serializeArray();
					var column_2 = jQuery("input[name='array_custom_1[]']").serializeArray();
					var column_3 = jQuery("input[name='array_custom_2[]']").serializeArray();
					var column_4 = jQuery("input[name='array_custom_3[]']").serializeArray();
					var column_5 = jQuery("input[name='array_custom_4[]']").serializeArray();
					var column_6 = jQuery("input[name='array_custom_5[]']").serializeArray();
					var column_7 = jQuery("input[name='array_custom_6[]']").serializeArray();
					var add_option = jQuery('input[name=add_option]:checked').val();
					var csrf = "{!! session()->get('_token') !!}";
					if(add_option && add_option == 'manual') {
						jQuery.ajax({
							url: "{{ url('recepients/manual') }}",
							type: 'POST',
							data: {
								table_name : '{{ Request::segment(3) }}',
								column_1 : column_1,
								column_2 : column_2,
								column_3 : column_3,
								column_4 : column_4,
								column_5 : column_5,
								column_6 : column_6,
								column_7 : column_7,
								_token : csrf
							},
							beforeSend: function() {
								jQuery('#ajax').modal('show');
							},
							success: function(response) {
								jQuery('#result-container').html(response);
							},
							complete: function() {
								jQuery('#ajax').modal('hide');
							},
							error: function(){}
						});
					}
				}
			}
			
		});
	</script>

	<script>
		  
		function csvChange(oInput) {
			var _validFileExtensions = [".csv"];
			var _element = $('#upload-text-container');  
			// alert('e');
			var file_name = '';
		    if (oInput.type == "file") {
		    	the_file = oInput.files[0];
		    	file_name = the_file.name;
		        var sFileName = oInput.value;
		         if (sFileName.length > 0) {
		            var blnValid = false;
		            for (var j = 0; j < _validFileExtensions.length; j++) {
		                var sCurExtension = _validFileExtensions[j];
		                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
		                    blnValid = true;
		                    break;
		                }
		            }
		             
		            if (!blnValid) {
		            	alert('ionvalid');
		                _element.css('color', 'red').html("Sorry, " + file_name + " is invalid, allowed extensions is: " + _validFileExtensions.join(", "));
		                oInput.value = "";
		                return false;
		            }
		        }
		    }
		    // return true;
		    jQuery('#ajax').modal('show');
			var form = new FormData(document.getElementById('submit_form'));
			var file = document.getElementById('csv-file').files[0];
			var csrf = "{!! session()->get('_token') !!}";
			var list_name = jQuery('input[name=list_name]').val();
			if(file) {
				form.append('csv_file', file);
				form.append('_token', "{{ csrf_token() }}");
				form.append('list_name', list_name);
			}
			jQuery.ajax({
				url: "{{ url('recepients/upload') }}",
				type: 'POST',
				data: form,
				contentType: false,
				processData: false,
				beforeSend: function() {},
				success: function(response) {
					jQuery('#result-container').html(response);
					_element.css('color', 'black').html(file_name);
				},
				complete: function() {
					jQuery('#ajax').modal('hide');
				},
				error: function() {}
			});
		}
	</script>

	<script>
		$('#form_wizard_1 .button-submit').click(function () {
			$('#ajax').modal('show');
			var is_update = jQuery('input[name=update]').val();
			
			if( ! is_update) {
				var data = {
					type : $('input[name=add_option]:checked').val(),
					table_name : $('input[name=list_name]').val(),
					_token : '{{ csrf_token() }}',
					filename : $('#upload-text-container').html()
				};
				$.post("{{ url('recipients/save') }}", data, function() {
					$('#ajax').modal('hide');
	                 		window.location.href = "{{ url('recepients/manage') }}";
				});
			} else {
				var data = {
					type : $('input[name=add_option]:checked').val(),
					table_name : $('input[name=list_name]').val(),
					id : $('input[name=update]').val(),
					from_table : '{{ Request::segment(3) }}',
					_token : '{{ csrf_token() }}',
					filename : $('#upload-text-container').html()
				};
				$.post("{{ url('recipients/update') }}", data, function() {
					$('#ajax').modal('hide');
	                 		window.location.href = "{{ url('recepients/manage') }}";
				});
			}			
			// $.post("{{ url('audit') }}", {action:'created a new recipient', _token:'{{ csrf_token() }}'}, function() {
			// 	$('#ajax').modal('hide');
   			//	window.location.href = "{{ url('recepients/manage') }}";
			// });
            }).hide();
	</script>

	@if(isset($table_name))
		<script>
			jQuery('.pagination a').on('click', function(e){
				jQuery('#ajax').modal('show');
				e.preventDefault();
				var url = $(this).attr('href');
				var list_name = jQuery('input[name=table_name]').val();
				var csrf = "{!! session()->get('_token') !!}";

				jQuery.ajax({
					url: url,
					type: 'POST',
					data: {page_link: url, table_name: list_name, _token: csrf},
					success: function(response) {
						jQuery('#result-container').html(response);
						jQuery('#ajax').modal('hide');
					},
					error: function() {}
				})
			});
		</script>
	@endif
@stop

@section('defined-scripts')
	<script>
	jQuery(document).ready(function() {       
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		FormWizard.init();
	});
	</script>
@stop