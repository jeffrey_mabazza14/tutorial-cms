@extends('layouts.default')
@section('styles')
	<link href="{{ asset('assets/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Home</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa icon-envelope-open"></i>
						<span>Campaign</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-plus"></i>
						<span>{{ isset($marketing) ? 'Update' : 'Create' }}</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box yellow-lemon" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-plus"></i> Campaign - {{ isset($marketing) ? 'Update' : 'Create' }}
							</div>
						</div>
						<div class="portlet-body form">
							<form action="#" class="form-horizontal" id="submit_form" method="POST">
								<input type="hidden" name="id" value="{{ isset($marketing) ? $marketing->id : '' }}">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Setup </span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
												<span class="number">
												2 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Select Recepients </span>
												</a>
											</li>
											<li>
												<a href="#tab3" data-toggle="tab" class="step active">
												<span class="number">
												3 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Design </span>
												</a>
											</li>
											<li>
												<a href="#tab4" data-toggle="tab" class="step">
												<span class="number">
												4 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Verify & Schedule </span>
												</a>
											</li>
										</ul>
										<div class="tab-content">
											{{-- <div class="alert alert-danger display-none">
												<button class="close" data-dismiss="alert"></button>
												You have some form errors. Please check below.
											</div> --}}
											<div class="alert alert-success display-none">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div>
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Campaign Info</h3>
												<div class="form-group">
													<label class="control-label col-md-3">Marketing Email Title <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="title" value="{{ isset($marketing) ? $marketing->title : '' }}" required />
														<span class="help-block" style="color: #737373 !important">
														This name doesn't display in your emails. It's only to help you identify each Campaign. </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Pick a Sender Address <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
															<select class="form-control" name="sender_id" required>
																@if($senders)
																	@foreach($senders as $sender)
																		<option {{ isset($marketing) && $marketing->sender_id == $sender->id ? 'selected' : '' }} value="{{ $sender->id }}" data-from-name="{{ $sender->from_name }}" data-reply-to="{{ $sender->reply_to }}">{{ $sender->description .' - '. $sender->from_name .', '. $sender->from_email .', '. $sender->address .', '. $sender->city .', '. $sender->state .', '. $sender->zip .', '. $sender->country }}</option>
																	@endforeach
																@else
																	<option></option>
																@endif
															</select>
														<span class="help-block" style="color: #737373 !important">
														Name of entity that will be used to send Marketing Email. You can {!! Html::link('sender/manage', 'edit existing ones', ['target'=>'_blank']) !!}. </span>
													</div>
												</div>
												{{-- <h3 class="block">Marketing Email Type</h3>
												<div class="form-group">
													<label class="control-label col-md-3">Choose Content Type <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<div class="icheck-inline radio-list">
															<label>
															<input type="radio" name="content_type" value="plain" checked="checked" />
															Plain text only </label>
															<label>
															<input type="radio" name="content_type" value="html" disabled />
															Manual </label>
														</div>
														<span class="help-block">
														Pick the content type that best suits your Marketing Email. </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Choose how to send your Marketing Email <span class="required">
													* </span>
													</label>
													<div class="col-md-6">
														<div class="icheck-inline radio-list">
															<label>
															<input type="radio" name="send_type" value="plain" disabled />
															Send the same Marketing Email to all recipients </label>
															<label>
															<input type="radio" name="send_type" value="html" disabled />
															Split Test my Marketing Email </label>
														</div>
													</div>
												</div> --}}
											</div>
											<div class="tab-pane" id="tab2">
												<h3 class="block">Select your Recepients Lists</h3>
												<button type="button" class="btn blue btn-sm" data-toggle="modal" data-target="#large">Select Recepients</button>
												{!! Html::link('recepients/create', 'Create a List', ['class'=>'btn blue btn-sm', 'target'=>'_blank']) !!}
													<table class="table table-bordered table-condensed flip-content" style="margin-top: 5px">
														<thead class="flip-content">
															<tr>
																<th>Recepient List Name</th>
																<th>No. of Contacts</th>
															</tr>
															</thead>
															<tbody id="recepients-container">
																<tr id="recepients-selected-list"></tr>
																@if(isset($marketing) && unserialize($marketing->recepients))
																	@for($i = 0; $i < count(unserialize($marketing->recepients)); $i++)
																		<tr>
																			<td>{{ str_replace('_', ' ', unserialize($marketing->recepients)[$i]) }}</td>
																			<td>{{ DB::table('client_'.unserialize($marketing->recepients)[$i])->count() }}</td>
																		</tr>
																	@endfor
																@endif
															</tbody>
														</table>
											</div>
											<div class="tab-pane" id="tab3">
												<h3 class="block">Design</h3>
												<div class="form-group">
													<label class="control-label col-md-3">Subject <span class="required">
													* </span>
													</label>
													<div class="col-md-6">
														<input type="text" class="form-control" name="subject" value="{{ isset($marketing) ? $marketing->subject : '' }}" required />
														<span class="help-block">
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">
													</label>
													<div class="col-md-6">
														<textarea id="txtarea" name="html_content" class="form-control ckeditor" rows="10" style="width: 100%" required>{{ isset($marketing) ? $marketing->html_content : '' }}</textarea>
														<span class="help-block" style="display: block; color: #737373 !important">
															Use {COLUMN_NAME} for data substitution. ex: Hello there, {NAME}!
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">
													</label>
													<div class="col-md-6">
														<button type="button" class="btn yellow-lemon" data-toggle="modal" data-target="#preview" id="html-preview">Preview</button>
														<span class="help-block">
														</span>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab4">
												<h3 class="block">Verify and Schedule</h3>
												<h4 class="form-section">Sending Options</h4>
												<div class="form-group">
													<label class="control-label col-md-3"></label>
													<div class="col-md-4">
														<div class="icheck-inline radio-list">
															<label>
															<input type="radio" name="schedule_options" value="send_now" checked="checked" onchange="changeSchedule('now')" {{ isset($marketing) && $marketing->schedule_options == 'send_now' ? 'checked' : '' }} />
															Send Now </label>
															<label>
															<input type="radio" name="schedule_options" value="send_later" onchange="changeSchedule('later')" {{ isset($marketing) && $marketing->schedule_options == 'send_later' ? 'checked' : '' }} />
															Send Later </label>
															<label>
															<input type="radio" name="schedule_options" value="draft" onchange="changeSchedule('draft')" {{ isset($marketing) && $marketing->schedule_options == 'draft' ? 'checked' : '' }} />
															Draft </label>
														</div>
													</div>
												</div>
												<div id="schedule" style="{{ isset($marketing) && $marketing->schedule_options == 'send_later' ? '' : 'display:none' }}">
													<div class="form-group">
														<label class="control-label col-md-3">Date</label>
														<div class="col-md-3">
															<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
																<input type="text" class="form-control" readonly name="date" value="{{ isset($marketing) ? $marketing->date : '' }}">
																<span class="input-group-btn">
																<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
																</span>
															</div>
															<!-- /input-group -->
															<span class="help-block">
															Select date </span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Time</label>
														<div class="col-md-2">
															<div class="input-icon">
																<i class="fa fa-clock-o"></i>
																<input type="text" class="form-control timepicker timepicker-default" name="time" value="{{ isset($marketing) ? $marketing->time : '' }}">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<a href="javascript:;" class="btn default button-previous">
												<i class="m-icon-swapleft"></i> Back </a>
												<a href="javascript:;" class="btn blue button-next">
												Continue <i class="m-icon-swapright m-icon-white"></i>
												</a>
												<a href="javascript:;" class="btn yellow-lemon button-submit">
												Submit <i class="m-icon-swapright m-icon-white"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

	<!-- START MODALS -->
	<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Select Recepient(s)</h4>
				</div>
				<div class="modal-body">
					 <table class="table table-bordered table-condensed flip-content" id="selected-recepients">
						<thead class="flip-content">
							<tr>
								<th></th>
								<th>Recepient List Name</th>
								<th>No. of Contacts</th>
							</tr>
							</thead>
							<tbody>
								@if($lists)
									@foreach($lists as $k => $v)
										<tr>
											<td><input type="checkbox" name="recepient" value="{{ $v['table'] }}" data-name="{{ $v['name'] }}" data-count="{{ $v['count'] }}"></td>
											<td>{{ $v['name'] }}</td>
											<td>{{ $v['count'] }}</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="15">You haven't selected a recepient(s) yet!</td>
									</tr>
								@endif								
							</tbody>
						</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<button type="button" class="btn blue" id="confirm-recepients">Save changes</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

	<div class="modal fade bs-modal-lg" id="preview" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title"><strong>From:</strong> <span id="from"></span></h4>
					<h4 class="modal-title"><strong>Subject:</strong> <span id="subject"></span></h4>
					<h4 class="modal-title"><strong>Reply:</strong> <span id="reply"></span></h4>
				</div>
				<div class="modal-body">
					 <div id="html-content"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

	<div class="modal fade" id="ajax" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<img src="../../assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
					<span>
					&nbsp;&nbsp;Sending... </span>
				</div>
			</div>
		</div>
	</div>
	<!-- END MODALS -->
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript" src="{{ asset('assets/global/plugins/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/global/plugins/ckfinder/ckfinder.js') }}"></script>
@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/form-wizard.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/components-pickers.js') }}" type="text/javascript"></script>

	<script>
		$(function() {
			jQuery('#from').html(jQuery(this).find(':selected').data('from-name'));
			jQuery('#reply').html(jQuery(this).find(':selected').data('reply-to'));
		})

		jQuery('select[name=sender_id]').on('change', function() {
			jQuery('#from').html(jQuery(this).find(':selected').data('from-name'));
			jQuery('#reply').html(jQuery(this).find(':selected').data('reply-to'));
		});

		jQuery('#confirm-recepients').on('click', function() {
			// var data = jQuery('#selected-recepients :checkbox').serialize();
			jQuery('#recepients-container').html('<tr id="recepients-selected-list"></tr>');
			jQuery('#selected-recepients :checkbox:checked').each(function() {
				jQuery('#recepients-selected-list').after('<tr><td>'+ jQuery(this).data('name') +'</td><td>'+ jQuery(this).data('count') +'</td></tr>');
			});
			jQuery('#large').modal('hide');
		});

		jQuery('#html-preview').on('click', function() {
			for (instance in CKEDITOR.instances) {
			    CKEDITOR.instances[instance].updateElement();
			}
			jQuery('#subject').html(jQuery('input[name=subject]').val());
			jQuery('#html-content').html(jQuery('textarea[name=html_content]').val());
		});
	</script>

	<script>
		$('#form_wizard_1 .button-submit').click(function () {
			for (instance in CKEDITOR.instances) {
			    CKEDITOR.instances[instance].updateElement();
			}
			jQuery('#ajax').modal('show');
			var recepients = [];
			var isUpdate = jQuery('input[name=id]').val() ? true : false;
			jQuery('input[name=recepient]:checked').each(function() {
				recepients.push(jQuery(this).val());
			});
			if(isUpdate === false) {
				var inputs = {
					method: 'add',
					title: jQuery('input[name=title]').val(),
					sender_id: jQuery('select[name=sender_id]').val(),
					content_type: jQuery('input[name=content_type]').val(),
					subject: jQuery('input[name=subject]').val(),
					html_content: jQuery('textarea[name=html_content]').val(),
					schedule_options: jQuery('input[name=schedule_options]:checked').val(),
					recepients: recepients,
					date: jQuery('input[name=date]').val() ? jQuery('input[name=date]').val() : false,
					time: jQuery('input[name=time]').val() ? jQuery('input[name=time]').val() : false,
					_token: "{!! session()->get('_token') !!}"
				};
			} else {
				var inputs = {
					method: 'update',
					id: jQuery('input[name=id]').val(),
					title: jQuery('input[name=title]').val(),
					sender_id: jQuery('select[name=sender_id]').val(),
					content_type: jQuery('input[name=content_type]').val(),
					subject: jQuery('input[name=subject]').val(),
					html_content: jQuery('textarea[name=html_content]').val(),
					schedule_options: jQuery('input[name=schedule_options]:checked').val(),
					recepients: recepients,
					date: jQuery('input[name=date]').val() ? jQuery('input[name=date]').val() : false,
					time: jQuery('input[name=time]').val() ? jQuery('input[name=time]').val() : false,
					_token: "{!! session()->get('_token') !!}"
				};
			}
			
			var url;
			if(inputs.schedule_options == 'send_now') {
				url = "{{ url('api/send') }}";
			} else if(inputs.schedule_options == 'send_later') {
				url = "{{ url('marketing/save') }}";
			} else if(inputs.schedule_options == 'draft') {
				url = "{{ url('api/draft') }}";
			}

			jQuery.post(url, inputs, function() {
				jQuery.post("{{ url('audit') }}", {action:'created a new marketing email', _token:'{{ csrf_token() }}'}, function() {
					jQuery('#ajax').modal('hide');
					window.location.href = "{{ url('marketing/manage') }}";	
				});				
			});
            }).hide();

            function changeSchedule(param) {
            	var schedule_container = jQuery('#schedule');
            	switch(param) {
            		case 'now':
            			schedule_container.slideUp(500);
            			break;
            		case 'later':
            			schedule_container.slideDown(500);
            			break;
            		default:
            			schedule_container.slideUp(500);
            			break;
            	}
            }
	</script>
@stop

@section('defined-scripts')
	<script>


	jQuery(document).ready(function() {    

		var editor = CKEDITOR.replace(
		                      'txtarea',
		                      {
		                        toolbar: [
		                                    ['NewPage', 'Undo', 'Redo', 'Find', 'Replace', 'SelectAll', 'Bold', 'Underline', 'Italic', 'Strike', 'RemoveFormat', 'NumberedList', 'BulletedList', 'Outdent', 'Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock', 'Image', 'Table', 'HorizontalRule', 'Styles', 'Format', 'Font', 'FontSize', 'TextColor', 'Maximize', 'Source']
		                                 ],
		                      },
		                      {height: 550},{width:500}
		                    );
		CKFinder.setupCKEditor(editor, '/ckfinder/');

		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		FormWizard.init();
		ComponentsPickers.init();
	});
	</script>
@stop