@extends('layouts.template')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
			<div class="col-md-12">
				<!-- BEGIN SAMPLE TABLE PORTLET-->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							{{-- <i class="fa fa-shopping-cart"> --}}</i>Members <span class="badge badge-default">	{{$transactions->total() }} </span> 
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
							<table class="table table-striped table-bordered table-advance table-hover">
							<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Ref No
								</th>
								<th>
									Transaction No
								</th>
								<th>
									Amount
								</th>
								<th>
									Payment Status
								</th>
								<th>
									Action
								</th>
							</tr>
							</thead>
							<tbody>
							<!-- BEGIN SEARCH -->
								<form action="{{ url('/admin/members') }}" method="GET">
									<tr>
										<td></td>
										<td>	
											<input type="text" class="form-control form-filter input-sm" name="name" value="{{ $_GET['name'] or '' }}">
										</td>
										<td>	
											<input type="text" class="form-control form-filter input-xm" name="ref_no" value="{{ $_GET['ref_no'] or '' }}">
										</td>
										<td>
											<input type="text" class="form-control form-filter input-xm" name="transaction_no" value="{{ $_GET['transaction_no'] or '' }}">
										</td>
										<td></td>
										<td></td>
										<td>
											<button class="btn default btn-sm filter-submit margin-bottom yellow-stripe"><i class="fa fa-search"></i> Search</button>
										</td>
									</tr>
								</form>
							<!-- END SEARCH -->
								<?php $each = ($transactions->currentPage() - 1) * 2 ?>
								@forelse($transactions as $transaction)
									<tr>
										<td>
											{{ $each + 1 }}
										</td>
										<td>
											{{ $transaction->name }}
										</td>
										<td>
											{{ $transaction->ref_no }}
										</td>
										<td>
											{{ $transaction->transaction_no }}
										</td>
										<td>
											{{ $transaction->amount }}
										</td>
										<th>
											{{ $transaction->paymentStatus }}
										</th>
										<th>
											<a href="#activate-{{ $transaction->id }}" class="btn default btn-sm green-stripe" data-toggle="modal">
											Activate </a>
											<a href="javascript:;" class="btn default btn-sm red-stripe">
											Reject </a>
										</th>

										<!-- BEGIN MODALS -->
										<div id="activate-{{ $transaction->id }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation <small>[Member ID: <b>{{ $transaction->id }}</b>]</small></h4>
													</div>
													<div class="modal-body">
														<p>
															 Are you sure you want to <b>ACTIVATE</b> the account of <b>{{ $transaction->name }}</b>?
														</p>
													</div>
													<div class="modal-footer">
														<form method="POST" action="{{ url('admin/update_transaction') }}">
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<input type="hidden" name="transaction_id" value="{{ $transaction->id }}">
															<input type="hidden" name="action" value="1">
															<button type="submit" class="btn green">Ok</button>
														</form>
													</div>
												</div>
											</div>
										</div>
										<!-- END MODALS -->
									</tr>
									<?php $each++ ?>
								@empty
									<tr>
										<td colspan="10"><center>No record<small>(s)</small> found</center></td>
									</tr>
								@endforelse
							</tbody>
							</table>
						</div>
						<div style="float: right">
						{!! $transactions->appends($fragments)->render() !!}
						</div>
					</div>
				</div>
				<!-- END SAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>


@endsection

@section('scripts')

@endsection

@section('init')

@endsection