@extends('layouts.template')
@section('styles')
	<style type="text/css">
		.error {
			color: #a94442;
		}
	</style>
@endsection
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXTRAS PORTLET-->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							Code Generator
						</div>
					</div>
					<div class="portlet-body form">
							<form role="form" action="{{ url('admin/generator') }}" method="POST">
								<div class="form-body">
									<div class="form-group">
										<label>Quantity</label>
										<input class="form-control _quantity" type="text" placeholder="Input quantity to be generated" name="quantity" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required />
										@if(isset($errors))
											<span class="help-block error">
												@if($errors->errors->first('quantity'))
													&nbsp;&nbsp;&nbsp;<i class="fa fa-warning tooltips"> {{ $errors->errors->first('quantity') }}</i>
												@endif
											</span>
										@endif
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Generate</button>
								</div>
							</form>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script>
		$("form").on("submit", function() {
			$("._quantity").addClass("spinner").attr("readOnly", "readOnly");
		});
	</script>
@endsection

@section('init')

@endsection