@extends('layouts.template')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/global/plugins/bootstrap-summernote/summernote.css') }}">
@endsection
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXTRAS PORTLET-->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							{{ $caption }}
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal form-bordered" action="{{ \Request::url() }}" method="POST">
							<input type="hidden" value="{{ csrf_token() }}" name="_token">
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-11">
										<textarea name="content" id="summernote_1">
											{{ $content->content }}
										</textarea>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn green">Update</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin_assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_assets/admin/pages/scripts/components-editors.js') }}"></script>
@endsection

@section('init')
ComponentsEditors.init();
@endsection