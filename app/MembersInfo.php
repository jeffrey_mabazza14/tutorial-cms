<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembersInfo extends Model
{
   protected $table = 'members_info';

   protected $hidden = ['password'];

   protected $primaryKey = 'member_id';

   public function qwert()
   {
   	return $this->belongsToMany('App\MembersTransaction', 'member_id');
   }

   public static function details()
   {
   	$data = \DB::table('members_info as mi')->join('members_transaction as mt', 'mi.member_id', '=', 'mt.member_id');

   	return $data;
   }
}