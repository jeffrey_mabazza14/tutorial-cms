<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return \Hash::make('secret');
});

# Login Controller
Route::get('admin/login', 'Auth\CmsAuthController@Login');

# Auth
Route::post('admin/auth', 'Auth\CmsAuthController@Authenticate');

# Admin Content
Route::group(['middleware'=>'auth', 'prefix'=>'admin'], function() {
	Route::resource('/', 'DashboardController');
	Route::get('logout', function() {
		\Auth::logout();

		return redirect('admin/login');
	});

	Route::get('members', 'MembersController@index');

	Route::get('terms', 'ContentsController@index_terms');
	Route::post('terms', 'ContentsController@index_terms');

	Route::get('about', 'ContentsController@index_about');
	Route::post('about', 'ContentsController@index_about');

	Route::get('generator', 'GeneratorController@index');
	Route::post('generator', 'GeneratorController@store');

	Route::post('update_transaction', 'TransactionsController@change_status');
});

// Route::get('/admin/logout', 'Auth\CmsAuthController@Logout');