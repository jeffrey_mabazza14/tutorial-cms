<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MembersInfo;
use App\MembersTransaction;
use App\BuyCode;
use App\ApprovedLog as Log;

class TransactionsController extends Controller
{
    public function __construct()
    {
        define('ACTIVATE',  1);
        define('REJECT',    2);
    }

    public function change_status()
    {
        $transaction_id = \Request::input('transaction_id');
        $action = \Request::input('action');

        switch($action) {
            case ACTIVATE:
                $transaction = MembersTransaction::find($transaction_id)->toArray();
                if($transaction) {
                    $buy_code = BuyCode::where('is_used', '')->where('member_id', NULL)->first()->toArray();
                    $buyCode = BuyCode::find($buy_code['id']);
                    $buyCode->is_used = 1;
                    $buyCode->member_id = $transaction['member_id'];
                    $buyCode->save();

                    $member = MembersInfo::find($transaction['member_id']);
                    $member->active_buycode_id = $buy_code['value'];
                    $member->save();

                    $Transaction = MembersTransaction::find($transaction['id']);
                    $Transaction->status = 1;
                    $Transaction->buy_code_id = $buy_code['id'];
                    $Transaction->save();

                    $log = new Log;
                    $log->ref_no = $transaction['ref_no'];
                    $log->transaction_no = $transaction['transaction_no'];
                    $log->member_id = $member['member_id'];
                    $log->approved_by_admin_name = \Auth::user()->name;
                    $log->date_approved = date('Y-m-d h:i:s');
                    $log->save();
                }
            break;

            case REJECT:
            break;

            default:
            break;
        }

        return redirect()->back();
    }
}
