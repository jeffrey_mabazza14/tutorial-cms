<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// use App\Member;
use App\MembersInfo as Member;
use App\MembersTransactions as Transaction;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $fragments = [];
        if(\Request::except('page')) {
            foreach(\Request::except('page') as $request_key => $request_value) {
                if($request_value) {
                    $fragments[$request_key] = $request_value;
                }
            }
        }
        $transactions = \DB::table('members_transaction as mt')->select(\DB::raw("*, CONCAT(`first_name`, ' ', `last_name`) as name"))->join('members_info as mi', function($query) {
            $query->on('mt.member_id', '=', 'mi.member_id');
            if(\Request::input('name')) {
                $query->where(\DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', '%'. \Request::input('name') .'%');
            }
            if(\Request::input('ref_no')) {
                $query->where('ref_no', 'LIKE', '%'. \Request::input('ref_no') .'%');
            }
            if(\Request::input('transaction_no')) {
                $query->where('transaction_no', 'LIKE', '%'. \Request::input('transaction_no') .'%');
            }
            $query->whereNull('buy_code_id');
        })->paginate(10);
        return \View::make('pages.members')->with(compact('transactions', 'fragments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
