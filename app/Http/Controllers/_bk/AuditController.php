<?php namespace App\Http\Controllers;

use App\Audit;
use Request;

class AuditController extends Controller {

	public function __construct()
	{
		
	}

	public function audit()
	{
		$audit = new Audit;

		$audit->username = session()->get('user')['username'];
		$audit->email = session()->get('user')['email'];
		$audit->activity = 'User '. session()->get('user')['username'] .' has '. Request::input('action');

		var_dump($audit->save());
	}

}
