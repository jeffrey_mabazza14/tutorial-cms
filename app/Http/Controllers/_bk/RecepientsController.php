<?php namespace App\Http\Controllers;

use View;
use DB;
use Schema;
use Request;
use Html;
use App\Recipients;

class RecepientsController extends Controller {

	private $schema;
	private $schema_count;
	private $header_1;
	private $header_2;
	private $header_3;
	private $header_4;
	private $header_5;
	private $header_6;
	private $header_7;
	private $column_headers;

	public function __construct()
	{
		// $this->middleware('guest');
	}

	public function setup()
	{
		$recipients = json_decode(json_encode(DB::table('recipients_list')->get()),TRUE);
		$recipients = array_fetch($recipients, 'table_name');
		$rec = [];
		foreach($recipients as $k => $v) {
			$rec[$k] = str_replace('_', ' ', $v);
		}
		$recipients = json_encode($rec);

		return View::make('pages.recepients-setup')->with(compact('recipients'));
	}

	public function upload()
	{
		// echo '<pre>';
		// print_r($_FILES);
		// exit;
		$file = $_FILES['csv_file']['tmp_name'];
		$result = fopen($file, 'r');
		$this->schema = fgetcsv($result);
		$this->schema_count = count($this->schema) - 1;

		$table_name = 'client_'. str_replace(' ', '_', Request::input('list_name'));

		// echo '<pre>';
		// print_r($this->schema);
		// exit;

		if( ! Schema::hasTable($table_name)) {
			Schema::create($table_name, function($table) {
				for($i = 0; $i <= $this->schema_count; $i++) {
					$table->string(str_replace(' ', '', snake_case($this->schema[$i])), 150)->nullable();
				}
			});	
		} else {
			Schema::dropIfExists($table_name);
			Schema::create($table_name, function($table) {
				for($i = 0; $i <= $this->schema_count; $i++) {
					$table->string(str_replace(' ', '', snake_case($this->schema[$i])), 150)->nullable();
				}
			});
		}

		$column_names = Schema::getColumnListing($table_name);
		while( ! feof($result)) {
			$data = fgetcsv($result);

			$insert_data = [];
			$j = 0;
			foreach($column_names as $k => $v) {
				if($data[$j]) {
					$insert_data[$v] = $data[$j];
				}
				$j++;
			}
			DB::table($table_name)->insert($insert_data);
		}

		$fields = $columns = Schema::getColumnListing($table_name);
		$rows = DB::table($table_name)->paginate(15);
		$rows->setPath('paginate');

		return View::make('pages.ajax.upload-results')->with(compact('fields', 'rows', 'table_name'));
	}

	public function uploadListPagination()
	{
		$table_name = Request::input('table_name');
		$fields = $columns = Schema::getColumnListing(Request::input('table_name'));
		$rows = DB::table(Request::input('table_name'))->paginate(15);

		return View::make('pages.ajax.upload-results')->with(compact('fields', 'rows', 'table_name'));
	}

	public function manual()
	{
		$column_1 = array_pluck(Request::input('column_1'), 'value')[0] ? array_pluck(Request::input('column_1'), 'value') : NULL;
		$column_2 = array_pluck(Request::input('column_2'), 'value')[0] ? array_pluck(Request::input('column_2'), 'value') : NULL;
		$column_3 = array_pluck(Request::input('column_3'), 'value')[0] ? array_pluck(Request::input('column_3'), 'value') : NULL;
		$column_4 = array_pluck(Request::input('column_4'), 'value')[0] ? array_pluck(Request::input('column_4'), 'value') : NULL;
		$column_5 = array_pluck(Request::input('column_5'), 'value')[0] ? array_pluck(Request::input('column_5'), 'value') : NULL;
		$column_6 = array_pluck(Request::input('column_6'), 'value')[0] ? array_pluck(Request::input('column_6'), 'value') : NULL;
		$column_7 = array_pluck(Request::input('column_7'), 'value')[0] ? array_pluck(Request::input('column_7'), 'value') : NULL;

		if($column_1) {
			$this->column_headers[] = str_replace(' ', '_', snake_case(head($column_1)));
			array_forget($column_1, '0');
			$column_data[] = $column_1;
		}
		if($column_2) {
			$this->column_headers[] = str_replace(' ', '_', snake_case(head($column_2)));
			array_forget($column_2, '0');
			$column_data[] = $column_2;
		}
		if($column_3) {
			$this->column_headers[] = str_replace(' ', '_', snake_case(head($column_3)));
			array_forget($column_3, '0');
			$column_data[] = $column_3;
		}
		if($column_4) {
			$this->column_headers[] = str_replace(' ', '_', snake_case(head($column_4)));
			array_forget($column_4, '0');
			$column_data[] = $column_4;
		}
		if($column_5) {
			$this->column_headers[] = str_replace(' ', '_', snake_case(head($column_5)));
			array_forget($column_5, '0');
			$column_data[] = $column_5;
		}
		if($column_6) {
			$this->column_headers[] = str_replace(' ', '_', snake_case(head($column_6)));
			array_forget($column_6, '0');
			$column_data[] = $column_6;
		}
		if($column_7) {
			$this->column_headers[] = str_replace(' ', '_', snake_case(head($column_7)));
			array_forget($column_7, '0');
			$column_data[] = $column_7;
		}



		$table_name = 'client_'. str_replace(' ', '_', Request::input('table_name'));
		if( ! Schema::hasTable($table_name)) {
			Schema::create($table_name, function($table) {
				foreach($this->column_headers as $k => $v) {
					$table->string($v, 150)->nullable();
				}
			});	
		} else {
			Schema::drop($table_name);
			Schema::create($table_name, function($table) {
				foreach($this->column_headers as $k => $v) {
					$table->string($v, 150)->nullable();
				}
			});
		}

		for($i = 1; $i <= count($column_data[0]); $i++) {
			$insert_data = [];
			if($column_1) {
				@$insert_data[$this->column_headers[0]] = $column_1[$i];
			}
			if($column_2) {
				@$insert_data[$this->column_headers[1]] = $column_2[$i];
			}
			if($column_3) {
				@$insert_data[$this->column_headers[2]] = $column_3[$i];
			}
			if($column_4) {
				@$insert_data[$this->column_headers[3]] = $column_4[$i];
			}
			if($column_5) {
				@$insert_data[$this->column_headers[4]] = $column_5[$i];
			}
			if($column_6) {
				@$insert_data[$this->column_headers[5]] = $column_6[$i];
			}
			if($column_7) {
				@$insert_data[$this->column_headers[6]] = $column_7[$i];
			}
			DB::table($table_name)->insert($insert_data);
		}

		$fields = $columns = Schema::getColumnListing($table_name);
		$rows = DB::table($table_name)->paginate(15);
		$rows->setPath('paginate');

		return View::make('pages.ajax.manual-results')->with(compact('fields', 'rows', 'table_name'));
	}

	public function manage()
	{
		$lists = Recipients::paginate(15);
		$total = Recipients::count();

		// $tables = DB::select('SHOW TABLES');

		// $lists = [];
		// foreach($tables as $k => $v) {
		// 	if(strpos($_SERVER['REMOTE_ADDR'] == '192.168.10.1' ? $v->Tables_in_homestead : $v->Tables_in_storytec_cortex, 'client') !== FALSE) {
		// 		$lists[$k]['table'] = $_SERVER['REMOTE_ADDR'] == '192.168.10.1' ? $v->Tables_in_homestead : $v->Tables_in_storytec_cortex;			
		// 		$lists[$k]['name'] = ucwords(str_replace('_', ' ', str_replace('client_', '', $_SERVER['REMOTE_ADDR'] == '192.168.10.1' ? $v->Tables_in_homestead : $v->Tables_in_storytec_cortex)));
		// 		$lists[$k]['count'] = DB::table($_SERVER['REMOTE_ADDR'] == '192.168.10.1' ? $v->Tables_in_homestead : $v->Tables_in_storytec_cortex)->count();
		// 	}
		// }
		
		// $total = count($lists);
		return View::make('pages.recepients-manage')->with(compact('lists', 'total'));
	}

	public function edit($table)
	{
		$recipients = json_decode(json_encode(DB::table('recipients_list')->where('table_name', '!=', $table)->get()),TRUE);
		$recipients = array_fetch($recipients, 'table_name');
		$rec = [];
		foreach($recipients as $k => $v) {
			$rec[$k] = str_replace('_', ' ', $v);
		}
		$recipients = json_encode($rec);

		$table_data = DB::table('client_'.$table)->paginate(15);
		$table_data->setPath('paginate');

		// echo '<pre>';
		// print_r($table_data);
		// exit;
		// $table_header = array_flatten(json_decode(json_encode(DB::select("SELECT column_name FROM information_schema.columns WHERE table_name = '". $table ."'")), TRUE));
		$table_header = Schema::getColumnListing('client_'.$table);
		$header_count = count($table_header);
		// echo '<pre>';
		// print_r($table_header);
		// exit;
		$table_name = $table;
		// $table = ucwords(str_replace('_', ' ', str_replace('client_', '', $table)));
		$table = str_replace('_', ' ', $table);

		$table_details = Recipients::where('table_name', '=', str_replace(' ', '_', $table))->firstOrFail();

		return View::make('pages.recepients-setup')->with(compact('table_data', 'table_header', 'table', 'table_name', 'table_details', 'header_count', 'recipients'));
	}

	public function editListPagination()
	{
		$table_name = Request::input('table_name');
		$table_header = $columns = Schema::getColumnListing(Request::input('table_name'));
		$table_data = DB::table(Request::input('table_name'))->paginate(15);

		return View::make('pages.ajax.edit-results')->with(compact('table_header', 'table_data', 'table_name'));
	}

	public function delete($table)
	{
		Schema::dropIfExists('client_'.$table);

		$Recipients = Recipients::where(['table_name'=>$table]);
		$Recipients->delete();

		return redirect('recepients/manage');
	}

	public function save()
	{
		$Recipients = new Recipients;

		$Recipients->table_name = str_replace(' ', '_', Request::input('table_name'));
		$Recipients->type = Request::input('type');
		$Recipients->filename = Request::input('filename');

		$Recipients->save();
	}

	public function update()
	{

		$Recipients = Recipients::find(Request::input('id'));

		$Recipients->table_name = str_replace(' ', '_', Request::input('table_name'));
		$Recipients->type = Request::input('type');
		$Recipients->filename = Request::input('filename');

		$Recipients->save();

		if( ! Schema::hasTable('client_'.str_replace(' ', '_', Request::input('table_name')))) {
			Schema::rename('client_'.Request::input('from_table'), 'client_'.str_replace(' ', '_', Request::input('table_name')));
		}

	}

}