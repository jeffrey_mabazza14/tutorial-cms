<?php namespace App\Http\Controllers;

use View;
use Auth;
use App\User;
use Request;
use Hash;
use DB;
use DateTime;

class AccountController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$count = isset($_GET['page']) ? ($_GET['page'] - 1) * 15 + 1 : 1;
		if(session()->get('user')['id'] == 1) {
			$users = DB::table('users')
					->where('id', '!=', session()->get('user')['id'])
					->paginate(15);
			$total = DB::table('users')
					->where('id', '!=', session()->get('user')['id'])
					->count();
		} else {
			$users = DB::table('users')
					->where('id', '!=', session()->get('user')['id'])
					->where('under', '=', session()->get('user')['id'])
					->paginate(15);
			$total = DB::table('users')
					->where('id', '!=', session()->get('user')['id'])				
					->where('under', '=', session()->get('user')['id'])
					->count();
		}
		
		return View::make('pages.users')->with(compact('users', 'count', 'total'));
	}

	public function createAccountUnder()
	{
		$under_id = session('user')['id'];

		return View::make('pages.user-create', compact('under_id'));
	}

	public function createAccountUnderSubmit()
	{
		$User = new User;
		$User->firstname = Request::input('firstname');
		$User->lastname = Request::input('lastname');
		$User->email = Request::input('email');
		$User->password = Hash::make(Request::input('password'));
		$User->status = 1;
		$User->under = Request::input('under_id');
		$User->save();
		
		return redirect('users');
	}

	public function manageAction($id)
	{
		if(Request::input('action')) {
			switch (Request::input('action')) {
				case 'approve':
					DB::table('users')->where('id', '=', $id)->update(['status'=>1]);
					break;
				case 'suspend':
					DB::table('users')->where('id', '=', $id)->update(['status'=>2]);
					break;
				case 'reject':
					DB::table('users')->where('id', '=', $id)->update(['status'=>3]);
					break;
				case 'remove':
					DB::table('users')->where('id', '=', $id)->delete();
				default:
					break;
			}

			return redirect('users');
		}
	}

	public function changePassword()
	{
		
		$user_id = session('user')['id'];
		return View::make('pages.change-password', compact('user_id'));
	}

	public function confirmChangePassword()
	{
		if(Request::input('id')) {
			$User = User::find(Request::input('id'));
			$user_id = session('user')['id'];
			if( ! Hash::check(Request::input('old_password'), $User->password)) {
				$error_message = 'Your old password does not matched.';
				$new_password = Request::input('password');
				$retype_password = Request::input('repassword');
				return View::make('pages.change-password', compact('user_id', 'new_password', 'retype_password', 'error_message'));
			} else {
				$success_message = 'Your password has successfully updated.';

				$User->password = Hash::make(Request::input('password'));
				$User->save();

				return View::make('pages.change-password', compact('user_id', 'success_message'));
			}			
		}
	}

}
