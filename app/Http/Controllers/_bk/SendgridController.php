<?php namespace App\Http\Controllers;

require_once '../app/libraries/SendGrid.php';
require_once '../app/libraries/SendGrid/Email.php';
require_once '../app/libraries/SendGrid/Exception.php';
require_once '../app/libraries/SendGrid/Response.php';

use View;
use App\Sender;
use DB;
use Mail;
use SendGrid;
use Config;
use Request;
use App\Marketing;
use App\Report;

class SendgridController extends Controller {

	public function __construct()
	{
		// $this->middleware('guest');
	}

	public function send()
	{
		// switch (Request::input('method')) {
		// 	case 'add':
		// 		break;
		// 	case 'update':
		// 		break;
		// 	default:
		// 		break;
		// }
		$marketing = new Marketing;

		if(Request::input('method') != 'update') {
			foreach(Request::except('_token', 'date', 'time', 'method', 'is_update') as $k => $v) {
				if($k == 'recepients') {
					$v = serialize($v);
				}
				$marketing->$k = $v;
			}
			$marketing->last_sent = date('Y-m-d H:i:s');

			$marketing->save();
		}		

		$sender = Sender::find(Request::input('sender_id'));

		if(Request::input('recepients')) {
			$sendgrid = new SendGrid(Config::get('constants.sendgrid.api_username'), Config::get('constants.sendgrid.api_password'));
			foreach(Request::input('recepients') as $k => $v) {
				$recepients = DB::table('client_'.$v)->get();

				$email_addresses = array_pluck($recepients, 'email_address');

				$substitution = $this->extract_text(Request::input('html_content'));

				// $template = "<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->";
				// $template .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
				// $template .= '<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">';
				// $template .= '<head>';
				// $template .= '<!-- If you delete this meta tag, Half Life 3 will never be released. -->';
				// $template .= '<meta name="viewport" content="width=device-width" />';
				// $template .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
				// $template .= '<title>Chooks to Go Email Template</title>';
				// $template .= '</head>';
				// $template .= '<body bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;"><style type="text/css">';
				// $template .= '@media only screen and (max-width: 600px) { a[class="btn"] { display: block !important; margin-bottom: 10px !important; background-image: none !important; margin-right: 0 !important;  } div[class="column"] { width: auto !important; float: none !important; } table.social div[class="column"] { width: auto !important;  }}';
				// $template .= '</style>';
				// $template .= '<!-- HEADER -->';
				// $template .= '<table class="head-wrap-top" bgcolor="#000000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; height: 15px; width: 100%; margin: 0; padding: 0;"></table><table class="head-wrap" bgcolor="#d20000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '<td class="header container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 690px !important; clear: both !important; margin: 0 auto; padding: 0;">';
				// $template .= '<div class="content large-logo-container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 690px; display: block; margin: -26px auto 0; padding: 15px 15px 8px;">';
				// $template .= '<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><img src="'.asset('assets/template/logo_large.png').'" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" /></td></tr></table></div></td>';
				// $template .= '<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '</tr></table><!-- /HEADER --><!-- BODY --><table class="body-wrap" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '<td class="container" bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 690px !important; clear: both !important; margin: 0 auto; padding: 0;">';
				// $template .= '<div class="content main-content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 690px; display: block; margin: 0 auto; padding: 100px 50px;">';
				// $template .= '<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">';
				$template = Request::input('html_content');
				// $template .= '</td></tr></table></div></td>';
				// $template .= '<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '</tr></table><!-- /BODY --><!-- FOOTER --><table class="head-wrap-top" bgcolor="#000000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; height: 15px; width: 100%; margin: 0; padding: 0;"></table><table class="head-wrap" bgcolor="#d20000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '<td class="header container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 690px !important; clear: both !important; margin: 0 auto; padding: 0;">';
				// $template .= '<div class="content small-logo-container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 690px; display: block; margin: -25px auto 0px; padding: 15px 15px 10px;">';
				// $template .= '<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><img src="'.asset('assets/template/logo_small.png').'" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" /></td>';
				// $template .= '</tr></table></div></td>';
				// $template .= '<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '</tr></table><!-- /FOOTER --></body>';
				// $template .= '</html>';

				$email = new SendGrid\Email();
				// $email->setAsmGroupId('44');
				$email->setSmtpapiTos($email_addresses);
				$email->setFrom($sender->from_email);
				$email->setReplyTo($sender->reply_to);
				$email->setFromName($sender->from_name);
				$email->setSubject(Request::input('subject'));
				// $email->setHtml(Request::input('html_content'));
				$email->setHtml($template);
				// $email->setHtml(' ');
				// $email->setTemplateId('231557eb-9790-4b32-9f55-2d847761481a');
				$email->addCategory(Request::input('title'));
				if($substitution) {
					foreach($substitution as $k => $v) {
						$email->addSubstitution('{'. strtoupper($v) .'}', array_pluck($recepients, $v));
					}
				}
				$response = $sendgrid->send($email);
				echo '<pre>';
				print_r($response);
			}
		}

		exit;

		

		echo '<pre>';
		print_r($response);
	}

	public function sendNow()
	{
		$marketing = Marketing::find(Request::input('id'));
		$result = $marketing->toArray();
		$sender = Sender::find($result['sender_id']);
		$recepients = unserialize($result['recepients']);
		if($recepients) {
			$sendgrid = new SendGrid(Config::get('constants.sendgrid.api_username'), Config::get('constants.sendgrid.api_password'));
			foreach($recepients as $k => $v) {
				$db_recepients = DB::table('client_'.$v)->get();

				$email_addresses = array_pluck($db_recepients, 'email_address');

				$substitution = $this->extract_text($result['html_content']);

				// $template = "<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->";
				// $template .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
				// $template .= '<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">';
				// $template .= '<head>';
				// $template .= '<!-- If you delete this meta tag, Half Life 3 will never be released. -->';
				// $template .= '<meta name="viewport" content="width=device-width" />';
				// $template .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
				// $template .= '<title>Chooks to Go Email Template</title>';
				// $template .= '</head>';
				// $template .= '<body bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;"><style type="text/css">';
				// $template .= '@media only screen and (max-width: 600px) { a[class="btn"] { display: block !important; margin-bottom: 10px !important; background-image: none !important; margin-right: 0 !important;  } div[class="column"] { width: auto !important; float: none !important; } table.social div[class="column"] { width: auto !important;  }}';
				// $template .= '</style>';
				// $template .= '<!-- HEADER -->';
				// $template .= '<table class="head-wrap-top" bgcolor="#000000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; height: 15px; width: 100%; margin: 0; padding: 0;"></table><table class="head-wrap" bgcolor="#d20000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '<td class="header container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 690px !important; clear: both !important; margin: 0 auto; padding: 0;">';
				// $template .= '<div class="content large-logo-container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 690px; display: block; margin: -26px auto 0; padding: 15px 15px 8px;">';
				// $template .= '<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><img src="'.asset('assets/template/logo_large.png').'" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" /></td></tr></table></div></td>';
				// $template .= '<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '</tr></table><!-- /HEADER --><!-- BODY --><table class="body-wrap" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '<td class="container" bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 690px !important; clear: both !important; margin: 0 auto; padding: 0;">';
				// $template .= '<div class="content main-content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 690px; display: block; margin: 0 auto; padding: 100px 50px;">';
				// $template .= '<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">';
				$template = $result['html_content'];
				// $template .= '</td></tr></table></div></td>';
				// $template .= '<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '</tr></table><!-- /BODY --><!-- FOOTER --><table class="head-wrap-top" bgcolor="#000000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; height: 15px; width: 100%; margin: 0; padding: 0;"></table><table class="head-wrap" bgcolor="#d20000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '<td class="header container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 690px !important; clear: both !important; margin: 0 auto; padding: 0;">';
				// $template .= '<div class="content small-logo-container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 690px; display: block; margin: -25px auto 0px; padding: 15px 15px 10px;">';
				// $template .= '<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><img src="'.asset('assets/template/logo_small.png').'" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" /></td>';
				// $template .= '</tr></table></div></td>';
				// $template .= '<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '</tr></table><!-- /FOOTER --></body>';
				// $template .= '</html>';

				$email = new SendGrid\Email();
				$email->addCategory($result['title']);
				$email->setSmtpapiTos($email_addresses);
				$email->setFrom($sender->from_email);
				$email->setReplyTo($sender->reply_to);
				$email->setFromName($sender->from_name);
				$email->setSubject($result['subject']);
				// $email->setHtml($result['html_content']);
				$email->setHtml($template);
				if($substitution) {
					foreach($substitution as $k => $v) {
						$email->addSubstitution('{'. strtoupper($v) .'}', array_pluck($db_recepients, $v));
					}
				}
				$response = $sendgrid->send($email);
				$marketing->last_sent = date('Y-m-d H:i:s');
				$marketing->save();
			}
		}
	}

	public function schedule()
	{
		$marketing = new Marketing;

		foreach(Request::except('_token', 'method') as $k => $v) {
			if($k == 'recepients') {
				$v = serialize($v);
			}
			$marketing->$k = $v;
		}

		$dateTime = date('Y-m-d', strtotime(Request::input('date'))). ' ' .date('H:i:s', strtotime(Request::input('time')));
		$marketing->last_sent = $dateTime;
		$marketing->save();

		$sender = Sender::find(Request::input('sender_id'));

		if(Request::input('recepients')) {
			$sendgrid = new SendGrid(Config::get('constants.sendgrid.api_username'), Config::get('constants.sendgrid.api_password'));
			foreach(Request::input('recepients') as $k => $v) {
				$recepients = DB::table('client_'.$v)->get();

				$email_addresses = array_pluck($recepients, 'email_address');

				$dateTime = date('Y-m-d', strtotime(Request::input('date'))). ' ' .date('H:i:s', strtotime(Request::input('time')));

				$substitution = $this->extract_text(Request::input('html_content'));

				// $template = "<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->";
				// $template .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
				// $template .= '<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">';
				// $template .= '<head>';
				// $template .= '<!-- If you delete this meta tag, Half Life 3 will never be released. -->';
				// $template .= '<meta name="viewport" content="width=device-width" />';
				// $template .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
				// $template .= '<title>Chooks to Go Email Template</title>';
				// $template .= '</head>';
				// $template .= '<body bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;"><style type="text/css">';
				// $template .= '@media only screen and (max-width: 600px) { a[class="btn"] { display: block !important; margin-bottom: 10px !important; background-image: none !important; margin-right: 0 !important;  } div[class="column"] { width: auto !important; float: none !important; } table.social div[class="column"] { width: auto !important;  }}';
				// $template .= '</style>';
				// $template .= '<!-- HEADER -->';
				// $template .= '<table class="head-wrap-top" bgcolor="#000000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; height: 15px; width: 100%; margin: 0; padding: 0;"></table><table class="head-wrap" bgcolor="#d20000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '<td class="header container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 690px !important; clear: both !important; margin: 0 auto; padding: 0;">';
				// $template .= '<div class="content large-logo-container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 690px; display: block; margin: -26px auto 0; padding: 15px 15px 8px;">';
				// $template .= '<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><img src="'.asset('assets/template/logo_large.png').'" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" /></td></tr></table></div></td>';
				// $template .= '<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '</tr></table><!-- /HEADER --><!-- BODY --><table class="body-wrap" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '<td class="container" bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 690px !important; clear: both !important; margin: 0 auto; padding: 0;">';
				// $template .= '<div class="content main-content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 690px; display: block; margin: 0 auto; padding: 100px 50px;">';
				// $template .= '<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">';
				$template = Request::input('html_content');
				// $template .= '</td></tr></table></div></td>';
				// $template .= '<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '</tr></table><!-- /BODY --><!-- FOOTER --><table class="head-wrap-top" bgcolor="#000000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; height: 15px; width: 100%; margin: 0; padding: 0;"></table><table class="head-wrap" bgcolor="#d20000" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '<td class="header container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 690px !important; clear: both !important; margin: 0 auto; padding: 0;">';
				// $template .= '<div class="content small-logo-container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 690px; display: block; margin: -25px auto 0px; padding: 15px 15px 10px;">';
				// $template .= '<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><img src="'.asset('assets/template/logo_small.png').'" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" /></td>';
				// $template .= '</tr></table></div></td>';
				// $template .= '<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>';
				// $template .= '</tr></table><!-- /FOOTER --></body>';
				// $template .= '</html>';

				$email = new SendGrid\Email();
				$email->setSmtpapiTos($email_addresses);
				$email->setFrom($sender->from_email);
				$email->setReplyTo($sender->reply_to);
				$email->setFromName($sender->from_name);
				$email->setSubject(Request::input('subject'));
				$email->setHtml($template);
				$email->setSendAt(strtotime((new \DateTime($dateTime))->format('c')));
				$email->addCategory(Request::input('title'));
				if($substitution) {
					foreach($substitution as $k => $v) {
						$email->addSubstitution('{'. strtoupper($v) .'}', array_pluck($recepients, $v));
					}
				}
				$response = $sendgrid->send($email);
				echo '<pre>';
				print_r($response);
			}
		}

		exit;

		

		echo '<pre>';
		print_r($response);
	}

	public function eventWebhook()
	{
		// $test = DB::table('postback')->first();
		// echo '<pre>';
		// print_r(json_decode($test->result, TRUE));
		// $r = json_decode('[{"email":"john.doe@sendgrid.com","sg_event_id":"VzcPxPv7SdWvUugt-xKymw","sg_message_id":"142d9f3f351.7618.254f56.filter-147.22649.52A663508.0","timestamp":1386636112,"smtp-id":"<142d9f3f351.7618.254f56@sendgrid.com>","event":"processed","category":["category1","category2","category3"],"id":"001","purchase":"PO1452297845","uid":"123456"},{"email":"not an email address","smtp-id":"<4FB29F5D.5080404@sendgrid.com>","timestamp":1386636115,"reason":"Invalid","event":"dropped","category":["category1","category2","category3"],"id":"001","purchase":"PO1452297845","uid":"123456"},{"email":"john.doe@sendgrid.com","sg_event_id":"vZL1Dhx34srS-HkO-gTXBLg","sg_message_id":"142d9f3f351.7618.254f56.filter-147.22649.52A663508.0","timestamp":1386636113,"smtp-id":"<142d9f3f351.7618.254f56@sendgrid.com>","event":"delivered","category":["category1","category2","category3"],"id":"001","purchase":"PO1452297845","uid":"123456"},{"email":"john.smith@sendgrid.com","timestamp":1386636127,"uid":"123456","ip":"174.127.33.234","purchase":"PO1452297845","useragent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36","id":"001","category":["category1","category2","category3"],"event":"open"},{"uid":"123456","ip":"174.56.33.234","purchase":"PO1452297845","useragent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36","event":"click","email":"john.doe@sendgrid.com","timestamp":1386637216,"url":"http://www.google.com/","category":["category1","category2","category3"],"id":"001"},{"uid":"123456","status":"5.1.1","sg_event_id":"X_C_clhwSIi4EStEpol-SQ","reason":"550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipients email address for typos or unnecessary spaces. Learn more at http: //support.google.com/mail/bin/answer.py?answer=6596 do3si8775385pbc.262 - gsmtp ","purchase":"PO1452297845","event":"bounce","email":"asdfasdflksjfe@sendgrid.com","timestamp":1386637483,"smtp-id":"<142da08cd6e.5e4a.310b89@localhost.localdomain>","type":"bounce","category":["category1","category2","category3"],"id":"001"},{"email":"john.doe@gmail.com","timestamp":1386638248,"uid":"123456","purchase":"PO1452297845","id":"001","category":["category1","category2","category3"],"event":"unsubscribe"}]', TRUE);
		// echo '<pre>';
		// print_r($r);
		$json = file_get_contents('php://input');
		// $json = json_decode($json, TRUE);
		// print_r($json);
		DB::table('postback')->insert(['result'=>$json]);
		// $result = file_get_contents('http://requestb.in/pw94fhpw');
  		// echo $result;

		$result = json_decode($json, TRUE);
		if($result) {
			foreach($result as $k => $v) {
				$Report = new Report;

				$Report->title = $v['category'][0];
				$Report->email = $v['email'];
				$Report->event = $v['event'];

				$Report->save();
			}
		}
	}

	public function eTest()
	{
		// $data = array('1@nuworks.ph', '2@nuworks.ph', '3@nuworks.ph');
		// $data = array_add($data, count($data), '4@nuworks.ph');
		// $data = array_add($data, count($data), '5@nuworks.ph');
		// $encode = serialize($data);
		// echo '<pre>';
		// print_r($data);
		// exit;
		$test = '[{"email":"ram.guiao@nuworks.ph","timestamp":1433822154,"smtp-id":"<14dd675b4fd.7170.154a6@ismtpd0015p1sin1>","response":"250 2.0.0 OK 1433822154 p12si8415260wjw.152 - gsmtp ","sg_event_id":"7QFqblcqQeyPiOF2FmbKkg","sg_message_id":"14dd675b4fd.7170.154a6.filter0465p1mdw1.32763.557663C430.1","category":["He Hi Ho Hu"],"event":"delivered"},
{"email":"jerome.jose@nuworks.ph","smtp-id":"<14dd675b4fd.7170.154a6@ismtpd0015p1sin1>","timestamp":1433822152,"sg_event_id":"WqeYsXDUR4Wgv9zV_rPrrA","sg_message_id":"14dd675b4fd.7170.154a6.filter0465p1mdw1.32763.557663C430.0","category":["He Hi Ho Hu"],"event":"processed"},
{"email":"ram.guiao@nuworks.ph","smtp-id":"<14dd675b4fd.7170.154a6@ismtpd0015p1sin1>","timestamp":1433822152,"sg_event_id":"x-bfI0IhT1O7CrX-nRI3QQ","sg_message_id":"14dd675b4fd.7170.154a6.filter0465p1mdw1.32763.557663C430.1","category":["He Hi Ho Hu"],"event":"processed"},
{"email":"jeffrey.mabazza@nuworks.ph","smtp-id":"<14dd675b4fd.7170.154a6@ismtpd0015p1sin1>","timestamp":1433822152,"sg_event_id":"1tW9MKr0SMe911T4yj3aoA","sg_message_id":"14dd675b4fd.7170.154a6.filter0465p1mdw1.32763.557663C430.2","category":["He Hi Ho Hu"],"event":"processed"}]
';
		$result = json_decode($test, TRUE);

		if($result) {
			foreach($result as $k => $v) {
				$Report = new Report;

				$Report->title = $v['category'][0];
				$Report->email = $v['email'];
				$Report->event = $v['event'];

				$Report->save();
			}
		}
	}

	public function reloadLastSent()
	{
		$result = Marketing::find(Request::input('id'));
		echo date('M d, Y h:i:s A', strtotime($result->last_sent));
	}

	private function extract_text($string)
	{
		$text_outside = [];
		$text_inside = [];
		$t = '';
		for($i = 0; $i < strlen($string); $i++) {
			if($string[$i] == '{') {
				$text_outside[] = $t;
				$t = '';
				$t1 = '';
				$i++;
				while($string[$i] != '}') {
					$t1 .= $string[$i];
					$i++;
				}
				$text_inside[] = strtolower($t1);
			} else {
				if($string[$i] != '}') {
					$t .= $string[$i];
				} else {
					continue;
				}
			}
		}
		if($t != '') {
			$text_outside[] = $t;
		}

		return $text_inside;
	}

}