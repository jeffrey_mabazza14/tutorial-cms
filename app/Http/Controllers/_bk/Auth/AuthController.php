<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use View;
use DB;
use App\User;
use Auth;
use Request;
use App\Audit;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function userLogin()
	{
		if( ! Auth::check()) {
			return View::make('layouts.login');
		} else {
			return redirect()->intended();
		}
	}

	public function userAuthenticate()
	{
		$audit = new Audit;

		if(Auth::attempt(['email'=>Request::input('username'), 'password'=>Request::input('password')])) {
			$user = DB::table('users')->where(['email'=>Request::input('username'), 'status'=>1])->first();

			if($user) {
				session()->put('user', [
					'id'=>$user->id,
					'username'=>$user->firstname,
					'firstname'=>$user->firstname,
					'lastname'=>$user->lastname,
					'email'=>$user->email
				]);
			} else {
				Auth::logout();
				return redirect('login')->with('error', ['message'=>'Your account is subject for approval by the Administrator.']);
			}
			// } else {
			// 	session()->put('user', [
			// 		'id'=>0,
			// 		'username'=>'Anonymous',
			// 		'firstname'=>'Anonymous',
			// 		'lastname'=>'Anonymous',
			// 		'email'=>'anonymous@email.com'
			// 	]);
			// }

			$audit->username = session()->get('user')['username'];
			$audit->email = session()->get('user')['email'];
			$audit->activity = 'User '. session()->get('user')['username'] .' has logged in';

			$audit->save();
			return redirect()->intended();
		} else {
			$user = DB::table('users')->where(['username'=>Request::input('username')])->first();
			if($user) {
				$audit->username = $user->username;
				$audit->email = $user->email;
				$audit->activity = $user->username .' failed login';
			} else {
				$audit->username = 'Anonymous';
				$audit->email = 'anonymous@email.com';
				$audit->activity = 'Anonymous user failed login';	
			}

			$audit->save();
			return redirect('login')->with('error', ['message'=>'Invalid username or password.']);
		}
	}

}
