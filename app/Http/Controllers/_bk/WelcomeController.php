<?php namespace App\Http\Controllers;

use View;
use Auth;
use App\User;
use App\Marketing;
use App\Report;
use Request;
use Hash;
use DB;
use DateTime;
use Mail;
use Vendor\Sendgrid\Sendgrid\Lib\Sendgrid;

class WelcomeController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		/* old */
		// $overall_metrics = json_decode(json_encode(DB::table('reports')->select('title', 'event', DB::raw('count(*) as total'))->groupBy('event')->get()), TRUE);
		$overall_recipients = json_decode(json_encode(DB::table('marketings')->where('last_sent', '<', date('Y-m-d H:i:s'))->where('last_sent', '!=', '0000-00-00 00:00:00')->get()),TRUE);

		$overall_metrics = [
			'processed'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'processed')->get()), TRUE)),
			'dropped'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'dropped')->get()), TRUE)),
			'delivered'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'delivered')->get()), TRUE)),
			'deferred'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'deferred')->get()), TRUE)),
			'bounce'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'bounce')->get()), TRUE)),
			'open'=>count(json_decode(json_encode(DB::table('reports')->groupBy('email')->groupBy('title')->where('event', '=', 'open')->get()), TRUE)),
			'click'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'click')->get()), TRUE)),
			'spam'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'spam')->get()), TRUE)),
			'unsubscribe'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'unsubscribe')->get()), TRUE))
		];

		$overall_metrics_up = [
			'processed'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'processed')->get()), TRUE)),
			'delivered'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'delivered')->get()), TRUE)),
			'open'=>count(json_decode(json_encode(DB::table('reports')->groupBy('email')->groupBy('title')->where('event', '=', 'open')->get()), TRUE)),
			'click'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'click')->get()), TRUE))
		];

		$overall_metrics_down = [
			'dropped'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'dropped')->get()), TRUE)),
			'deferred'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'deferred')->get()), TRUE)),
			'bounce'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'bounce')->get()), TRUE)),
			'spam'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'spam')->get()), TRUE)),
			'unsubscribe'=>count(json_decode(json_encode(DB::table('reports')->where('event', '=', 'unsubscribe')->get()), TRUE))
		];

		$count = 0;

		foreach($overall_recipients as $k => $v) {
			$recipients = unserialize($v['recepients']);
			foreach($recipients as $k) {
				$count = $count + DB::table('client_'.$k)->count();
			}
		}

		$overall_recipients = $count;

		$overall_metrics_up_percentage = [
			'processed'=>$overall_recipients && $overall_metrics_up['processed'] ? number_format(($overall_metrics_up['processed'] / $overall_recipients) * 100, 2) : '0.00',
			'delivered'=>$overall_recipients && $overall_metrics_up['delivered'] ? number_format(($overall_metrics_up['delivered'] / $overall_recipients) * 100, 2) : '0.00',
			'open'=>$overall_recipients && $overall_metrics_up['open'] ? number_format(($overall_metrics_up['open'] / $overall_recipients) * 100, 2) : '0.00',
			'click'=>$overall_recipients && $overall_metrics_up['click'] ? number_format(($overall_metrics_up['click'] / $overall_recipients) * 100, 2) : '0.00'
		];

		$overall_metrics_down_percentage = [
			'dropped'=>$overall_recipients && $overall_metrics_down['dropped'] ? number_format(($overall_metrics_down['dropped'] / $overall_recipients) * 100, 2) : '0.00',
			'deferred'=>$overall_recipients && $overall_metrics_down['deferred'] ? number_format(($overall_metrics_down['deferred'] / $overall_recipients) * 100, 2) : '0.00',
			'bounce'=>$overall_recipients && $overall_metrics_down['bounce'] ? number_format(($overall_metrics_down['bounce'] / $overall_recipients) * 100, 2) : '0.00',
			'spam'=>$overall_recipients && $overall_metrics_down['spam'] ? number_format(($overall_metrics_down['spam'] / $overall_recipients) * 100, 2) : '0.00',
			'unsubscribe'=>$overall_recipients && $overall_metrics_down['unsubscribe'] ? number_format(($overall_metrics_down['unsubscribe'] / $overall_recipients) * 100, 2) : '0.00'
		];

		// foreach($overall_metrics_up_percentage as $k => $v) {
		// 	print_r($v);
		// }
		// exit;


		$Marketing = json_decode(json_encode(DB::table('marketings')->where('last_sent', '<', date('Y-m-d H:i:s'))->where('last_sent', '!=', '0000-00-00 00:00:00')->orderBy('created_at', 'desc')->get()), TRUE);
		
		if($Marketing) {
			$title = $Marketing[0]['title'];
			$id = $Marketing[0]['id'];
			$final_graph = [];
			$total_email = DB::table('reports')->select('email')->where('title', '=', $Marketing[0]['title'])->distinct('email')->get();
			$total = count($total_email);
			$graph = json_decode(json_encode(DB::table('reports')->where('title', '=', $Marketing[0]['title'])->select('title', 'event', 'email', DB::raw('count(distinct(`email`)) as total'))->groupBy('title')->groupBy('event')->get()), TRUE);

			foreach($graph as $k => $v) {
				$percent = $v['total'] / $total; 
				$final_graph[$k]['title'] = $v['title'];
				$final_graph[$k]['event'] = $v['event'];
				$final_graph[$k]['total'] = $v['total'];
				$final_graph[$k]['percent'] = $percent;
				$final_graph[$k]['percent_friendly'] = number_format($percent * 100, 2);
			}
		}

		$audits = DB::table('audits')
				->orderBy('created_at', 'DESC')
				->take(15)
				->get();
		if($audits) {
			foreach($audits as $k => $v) {
				$audits[$k]->time_ago = $this->timeAgo($v->created_at);
			}
		}
		return View::make('pages.home')->with(compact('audits', 'final_graph', 'title', 'total', 'overall_metrics', 'overall_recipients', 'id', 'overall_metrics_up', 'overall_metrics_down', 'overall_metrics_up_percentage', 'overall_metrics_down_percentage'));
	}

	public function logOut()
	{
		Auth::logout();
		return redirect('login');
	}

	public function createUser()
	{
		$duplicate_email = User::where('email', '=', Request::input('email'))->get()->count();

		$user = new User;

		if($duplicate_email) {
			foreach(Request::except(['_token', 'rpassword', 'tnc']) as $k => $v) {
				$user->$k = $v = $k == 'password' ? Hash::make($v) : $v;
			}
			$user->status = 3;
		} else {
			foreach(Request::except(['_token', 'rpassword', 'tnc']) as $k => $v) {
				$user->$k = $v = $k == 'password' ? Hash::make($v) : $v;
			}	
		}

		if($user->save()) {
			return redirect('login')->with('success', ['message'=>'Registration complete. Your account is subject for approval.']);
		} else {
			return redirect('login')->with('error', ['message'=>'Registration failed. Please try again later.']);
		}
	}

	private function generatePassword($length = 10)
	{
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$count = mb_strlen($chars);

		for ($i = 0, $result = ''; $i < $length; $i++) {
			$index = rand(0, $count - 1);
			$result .= mb_substr($chars, $index, 1);
		}

		return $result;
	}

	public function forgotPassword()
	{
		if(Request::input('email')) {
			$User = User::where('email', '=', Request::input('email'))->where('status', '=', 1)->get()->toArray();
			if($User) {
				$new_password = $this->generatePassword();
				$UpdateUser = User::find($User[0]['id']);
				$UpdateUser->password = Hash::make($new_password);
				$UpdateUser->save();
				Mail::send('emails.welcome', ['password'=>$new_password], function($message) {
					$message->from('admin@cortex.storyteching.ph', 'Admin Cortex');
					$message->to(Request::input('email'))->subject('Cortex Forgot Password');
				});
				return redirect('login')->with('success', ['message'=>'Please check your email for your new password.']);
			} else {
				return redirect('login')->with('error', ['message'=>'You have entered and invalid email address.']);
			}			
		}
	}

	private function timeAgo($timestamp)
	{
		$datetime1 = new DateTime("now");
		$datetime2 = date_create($timestamp);
		$diff = date_diff($datetime1, $datetime2);
		$timemsg = '';
		if($diff->y > 0) {
			$timemsg = $diff->y .'y'. ($diff->y > 1?"":'');
		}
		else if($diff->m > 0) {
			$timemsg = $diff->m . 'month'. ($diff->m > 1?"":'');
		}
		else if($diff->d > 0) {
			$timemsg = $diff->d .'d'. ($diff->d > 1?"":'');
		}
		else if($diff->h > 0) {
			$timemsg = $diff->h .'h'.($diff->h > 1 ? "":'');
		}
		else if($diff->i > 0) {
			$timemsg = $diff->i .'m'. ($diff->i > 1?"":'');
		}
		else if($diff->s > 0) {
			$timemsg = $diff->s .'s'. ($diff->s > 1?"":'');
		}

		// $timemsg = $timemsg .' ago';
		return $timemsg;
	}

}
