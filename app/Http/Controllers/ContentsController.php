<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Content;

class ContentsController extends Controller
{
    public function index_terms()
    {
        if(\Request::input('content')) {
            $update_content = Content::findByType('terms');
            $update_content->content = \Request::input('content');
            $update_content->save();
        }        

        $content = Content::where('type', '=', 'terms')->first();
        $caption = 'Terms & Conditions';

        return \View::make('pages.contents')->with(compact('content', 'caption'));
    }

    public function index_about()
    {
        if(\Request::input('content')) {
            $update_content = Content::findByType('about');
            $update_content->content = \Request::input('content');
            $update_content->save();
        }

        $content = Content::where('type', '=', 'about')->first();
        $caption = 'About Us';

        return \View::make('pages.contents')->with(compact('content', 'caption'));
    }
}