<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GeneratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return \View::make('pages.generators');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
       if(\Request::input('quantity')) {
            $validator = \Validator::make(
                [
                    'quantity'=>\Request::input('quantity')
                ],
                [
                    'quantity'=>'required|integer'
                ],
                [
                    'integer'=>'This field should be a number.'
                ]
            );

            if( ! $validator->fails()) {
                $this->generate_code(\Request::input('quantity'));
            } else {
                return redirect('admin/generator')->withErrors($validator, 'errors');
            }

            return redirect('admin/generator');
       }
    }

    private function generate_code($qty)
    {
        for($i = 0; $i < $qty; $i++) {
            $code = $this->getRandomString();
            if( ! \DB::table('buy_codes')->where('value', $code)->first()) {
                \DB::table('buy_codes')->insert(['value'=>$code]);
            } else {
                $i--;
            }
        }
    }

    private function getRandomString($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
