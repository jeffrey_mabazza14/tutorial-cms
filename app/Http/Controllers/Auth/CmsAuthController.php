<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Auth;

class CmsAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function Authenticate()
    {
        if(Auth::attempt(['email'=>\Request::input('email'), 'password'=>\Request::input('password'), 'is_deleted'=>'0']))
        {
            $valid_email = User::where('email', '=', \Request::input('email'))->first();
            if($valid_email)
            {
                if($valid_email->is_blocked)
                {
                    if(date('Y-m-d H:i:s', strtotime('+2 minutes', strtotime($valid_email->blocked_at))) > date('Y-m-d H:i:s'))
                    {
                        $user = User::find($valid_email->id);
                        $user->is_blocked = 0;
                        $user->blocked_at = '0000-00-00 00:00:00';
                        $user->login_attempts = 0;
                        $user->save();
                    }
                    else
                    {
                        Auth::logout();
                        return redirect('admin/login')->with('error', 'This account is blocked. Please try again later.');
                    }
                }
                return redirect('admin/');
            }
        }
        else
        {
            $valid_email = User::where('email', '=', \Request::input('email'))->where('is_deleted', '=', '0')->first();
            if($valid_email)
            {
                if($valid_email->login_attempts < 3)
                {
                    $user = User::find($valid_email->id);
                    $user->increment('login_attempts');
                    $user->updated_at = date('Y-m-d H:i:s');
                    $user->save();

                    $message = "You have entered an invalid password.";
                }
                elseif($valid_email->login_attempts >= 3)
                {
                    $user = User::find($valid_email->id);
                    $user->login_attempts = 0;
                    $user->is_blocked = 1;
                    $user->blocked_at = date('Y-m-d H:i:s');
                    $user->updated_at = date('Y-m-d H:i:s');
                    $user->save();

                    $message = "Account has been blocked.";
                }
            }
            else
            {
                $message = "Email does not exist.";
            }
            return redirect('admin/login')->with('error', $message);
        }
    }

    public function Login()
    {
        if( ! Auth::check())
        {   
            return \View::make('layouts.login');
        }
        else
        {
            return redirect()->intended();
        }
    }

    public function Logout()
    {
        Auth::logout();
        return redirect('admin/login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
