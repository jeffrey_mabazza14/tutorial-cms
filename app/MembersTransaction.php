<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembersTransaction extends Model
{
    protected $table = "members_transaction";
}
