<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		// if($this->isHttpException($e)) {
		// 	switch ($e->getStatusCode()) {
		// 	// not found
		// 	case 404:
		// 	echo '<!DOCTYPE html><html><body><h4>Page does not exist. Redirecting in <span id="count-down">5</span></h4><script>var count = 5;setInterval(function() {if(count === 1) {window.location.href = "/"} count = count - 1; document.getElementById("count-down").innerHTML = count;}, 1000);</script></body></html>';
		// 	exit;
		// 	// return redirect()->guest('home');
		// 	// return redirect()->guest('home');

		// 	break;
		// 	// internal error
		// 	case '500':
		// 	return redirect()->guest('home');

		// 	break;

		// 	default:
		// 	return $this->renderHttpException($e);
		// 	break;
		// 	}
		// } else {
		// 	return parent::render($request, $e);
		// }
		return parent::render($request, $e);
	}

}
