<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyCode extends Model
{
    protected $table = "buy_codes";
}
