<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('marketings', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('sender_id');
			$table->string('title', 500);
			$table->string('content_type', 50);
			$table->string('subject', 100);
			$table->string('schedule_options', 50);
			$table->string('date', 50)->nullable();
			$table->string('time', 50)->nullable();
			$table->text('html_content');
			$table->text('recepients');
			$table->dateTime('last_sent');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('marketings');
	}

}
