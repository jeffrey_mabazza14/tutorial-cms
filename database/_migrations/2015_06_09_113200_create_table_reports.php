<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReports extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 100);
			$table->string('email', 100);
			$table->text('sendgrid_data');
			$table->string('event', 100);
			$table->bigInteger('processed')->default(0);
			$table->bigInteger('dropped')->default(0);
			$table->bigInteger('deferred')->default(0);
			$table->bigInteger('delivered')->default(0);
			$table->bigInteger('bounced')->default(0);
			$table->bigInteger('opened')->default(0);
			$table->bigInteger('clicked')->default(0);
			$table->bigInteger('unsubscribed_from')->default(0);
			$table->bigInteger('mark_as_spam')->default(0);
			$table->bigInteger('asm_group_unsubscribe')->default(0);
			$table->bigInteger('asm_group_resubscribe')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reports');
	}

}
