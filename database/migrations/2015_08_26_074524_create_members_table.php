<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members_info', function (Blueprint $table) {
            $table->increments('member_id');
            $table->string('email')->nullable(); //unique for LOGIN PURPOSES
            $table->string('password', 60)->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('buycode_package')->nullable();
            $table->string('active_buycode_id',100)->nullable();
            $table->string('buycode_status')->nullable();
            $table->dateTime('buycode_start_time')->nullable();
            $table->dateTime('buycode_end_time')->nullable();
            $table->date('birth_date')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->integer('contact_no')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });

         Schema::create('members_downline', function (Blueprint $table) {
            $table->increments('id');
            $table->string('is_paid')->nullable();
            $table->string('transaction_no')->nullable();
            $table->string('member_id')->nullable();
            $table->string('active_buycode_id')->nullable();
            $table->string('sponsor_buycode_id')->nullable();//dito lagi ung payment
            $table->string('upline_buycode_id')->nullable();//dito ung sa downline lang
            $table->string('position')->nullable();
            $table->string('package')->nullable();
            $table->string('status')->nullable();
            $table->date('date_start')->nullable();
            
      
            $table->timestamps();
        });

          Schema::create('members_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('buy_code_id')->nullable();
            $table->string('member_id')->nullable();
            $table->string('ref_no')->nullable();
            $table->string('status')->nullable();

            $table->string('transaction_no')->nullable();
            $table->integer('amount')->nullable();
            $table->string('paymentStatus')->nullable();
            $table->dateTime('paymentDate')->nullable();
            $table->string('paymentGateway')->nullable();
            $table->string('digest')->nullable();
            $table->string('message')->nullable();
            $table->dateTime('buyDate')->nullable();

            $table->dateTime('date_purchased')->nullable();
            $table->timestamps();
        });

            Schema::create('members_notification', function (Blueprint $table) {
            $table->increments('notification_id');
            $table->string('member_id')->nullable();
            $table->string('buy_code_id')->nullable();
            $table->longText('message')->nullable();
            $table->string('origin')->nullable();
            $table->string('suborigin')->nullable();
            $table->integer('is_read')->nullable();
            $table->integer('date_read')->nullable();
            $table->dateTime('is_deleted')->nullable();
            $table->timestamps();
        });

            Schema::create('audit_trail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id')->nullable();
            $table->string('buy_code_id')->nullable();
            $table->longText('message')->nullable();
            $table->string('ip_add')->nullable();
            $table->string('activity')->nullable();
            $table->dateTime('date_created')->nullable();
            $table->timestamps();
        });


            Schema::create('approved_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_no')->nullable();
            $table->string('transaction_no')->nullable();
            $table->string('member_id')->nullable();
            $table->string('message')->nullable();
            $table->string('approved_by_admin_name')->nullable();
            $table->dateTime('date_approved')->nullable();
            $table->dateTime('date_created')->nullable();
            $table->timestamps();
        });

            Schema::create('buy_codes', function (Blueprint $table) {
                $table->increments('id');
                $table->string('value')->unique();
                $table->integer('member_id')->nullable();
                $table->integer('is_used');
                $table->timestamps();
            });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('members');
         Schema::drop('members_tree');
         Schema::drop('members_transaction');
    }
}
