<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        # CMS USERS
        $cms_users = [
            'name'=>'Super Administrator',
            'email'=>'superadmin@nuworks.ph',
            'password'=>\Hash::make('superadmin'),
            'role'=>2
        ];
        DB::table('cms_users')->truncate();
        DB::table('cms_users')->insert($cms_users);

        # MEMBERS INFO
        $members_info = [
            [
                'first_name'=>'Jerome',
                'last_name'=>'Jose',
                'email'=>'jerome.jose@yahoo.com',
                'password'=>bcrypt('p@ssw0rd')
            ],
            [
                'first_name'=>'Doms',
                'last_name'=>'Garcia',
                'email'=>'doms.garcia@yahoo.com',
                'password'=>bcrypt('p@ssw0rd'),
            ],
            [
                'first_name'=>'Jeffrey',
                'last_name'=>'Mabazza',
                'email'=>'jeffrey.mabazza@nuworks.ph',
                'password'=>bcrypt('p@ssw0rd')
            ]
        ];
        DB::table('members_info')->truncate();
        DB::table('members_info')->insert($members_info);

        # CONTENTS
        $contents = [
            [
                'type'=>'terms',
                'content'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            ],
            [
                'type'=>'about',
                'content'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            ]
        ];
        DB::table('contents')->truncate();
        DB::table('contents')->insert($contents);

        $members_transactions = [
            [
                'member_id'=>'1',
                'ref_no'=>'1234',
                'paymentStatus'=>'P'
            ],
            [
                'member_id'=>'2',
                'ref_no'=>'2345',
                'paymentStatus'=>'S'
            ]
        ];
        DB::table('members_transaction')->truncate();
        DB::table('members_transaction')->insert($members_transactions);

        Model::reguard();
    }
}
