<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Sender;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		DB::table('users')->truncate();

		// Users
		User::create([
			'username'=>"admin@nuworks.ph",
			'password'=>'$2y$10$TfDHa71Ee9fh2/ds86p/n.sHqsUxOwaEqVbbP5Nip/0MUGKib0CTa',
			'firstname'=>"Aministrator",
			'lastname'=>"Seeder",
			'email'=>'admin@nuworks.ph',
			'rights'=>1,
			'status'=>1
		]);
		User::create([
			'username'=>"tester@nuworks.ph",
			'password'=>'$2y$10$LbJIMMsOew0umV86wZws2OX2V7BK1eP41TWgs6iSPXAwQOstbiBx6',
			'firstname'=>"Tester",
			'lastname'=>"Seeder",
			'email'=>'tester@nuworks.ph',
			'rights'=>1,
			'status'=>1
		]);

		// Sender
		Sender::create([
			'from_name'=>'NuWorks Tester',
			'from_email'=>'noreply@nwshare.ph',
			'reply_to'=>'donotreply@nwshare.ph',
			'description'=>'NuWorks Tester',
			'address'=>'One Corporate, Meralco Avenue corner Julia Vargas Avenue, Ortigas Center',
			'city'=>'Pasig City',
			'state'=>'Metro Manila',
			'zip'=>1605,
			'country'=>'PH'
		]);

		// $this->call('UserTableSeeder');
	}

}
